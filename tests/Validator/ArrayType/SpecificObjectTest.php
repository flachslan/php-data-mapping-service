<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Tests\Dummy\{Class1,
    Class2,
    DummyAbstractClass,
    UseStatements\ClassWithStdClassObject};
use Oktavlachs\DataMappingService\Validator\ArrayType\SpecificObject;
use PHPUnit\Framework\TestCase;

/**
 * Class SpecificObjectTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class SpecificObjectTest extends TestCase
{
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            SpecificObject::class,
            new SpecificObject('int', Class2::class)
        );
    }

    public function testConstructThrowsExceptionOnInvalidIndexType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new SpecificObject('sdtClass', 'string');
    }

    public function testConstructThrowsExceptionOnNonObjectElementType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new SpecificObject('int', 'object');
    }

    public function testConstructDoesntAllowAbstractClasses(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new SpecificObject('int', DummyAbstractClass::class);
    }

    public function testValidateWithValidObjects(): void
    {
        $validator = new SpecificObject('int', Class2::class);

        $expected = [new Class2(), new Class2()];

        $this->assertSame(
            $expected,
            $validator->validate($this->dataMappingService, $expected)
        );
    }

    public function testValidateWithValidObjectsInArrayRepresentation(): void
    {
        $validator = new SpecificObject('int', Class2::class);

        $class1AsArray = [
            'someString' => 'gun',
            'scalarNestedArray' => [['someIndex' => false]],
            'nullableNestedArray' => null
        ];

        $class2AsArray = [
            // These are inherited of class 1
            'someString' => 'inClass2',
            'scalarNestedArray' => [],
            'nullableNestedArray' => [],
            // These are defined by class 2
            'userDefinedObjectAsAlias' => $class1AsArray,
            'arrayWithObjectsWithoutFullNamespace' => [
                [[]],
                [new ClassWithStdClassObject()]
            ],
            'arrayWithObjectsAsAliases' => [[new Class1()], [$class1AsArray]]
        ];

        $collectionOfClass2Elements = [$class2AsArray, $class2AsArray, $class2AsArray];

        foreach (
            $validator->validate(
                $this->dataMappingService,
                $collectionOfClass2Elements
            ) as $result
        ) {
            $this->assertInstanceOf(Class2::class, $result);
        }
    }

    public function testValidateThrowsExceptionOnWrongValueElementType(): void
    {
        $validator = new SpecificObject('int', Class2::class);

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, [3.3]);
    }

    public function testValidateThrowsExceptionOnWrongValueType(): void
    {
        $validator = new SpecificObject('int', Class2::class);

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, 3);
    }

    public function testValidateThrowsExceptionOnWrongValueIndexType(): void
    {
        $validator = new SpecificObject('int', Class2::class);

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, ['someStringIndex' => 3]);
    }
}
