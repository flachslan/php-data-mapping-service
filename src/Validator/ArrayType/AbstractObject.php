<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\ArrayType;

use Oktavlachs\DataMappingService\DataMappingService;
use ReflectionClass;
use ReflectionException;
use InvalidArgumentException;
use Oktavlachs\DataMappingService\Validator\AbstractPropertyTypeValidator;

/**
 * A validator for array with elements of an abstract object type.
 *
 * @package Oktavlachs\DataMappingService\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class AbstractObject extends AbstractPropertyTypeValidator
{
    private string $arrayIndexType;

    private string $abstractArrayElementType;

    /**
     * AbstractObject constructor.
     *
     * @param string $arrayIndexType The type that each index of the array
     * must be of (probably int or string)
     * @param string $abstractArrayElementType The type that each element
     * of the array must be of
     */
    public function __construct(
        string $arrayIndexType,
        string $abstractArrayElementType
    ) {
        $this->validateArrayIndexType($arrayIndexType);

        $this->arrayIndexType = $arrayIndexType;

        try {
            $abstractArrayElementClass = new ReflectionClass($abstractArrayElementType);
        } catch (ReflectionException $e) {
            $errorMessage = sprintf(
                "Abstract array element type '%s' is not supported.",
                $abstractArrayElementType
            );

            throw new InvalidArgumentException($errorMessage, $e->getCode(), $e);
        }

        if (!$abstractArrayElementClass->isAbstract()) {
            $errorMessage = sprintf(
                'Abstract class expected as array element type. '
                . "Given array element type: '%s'.",
                $abstractArrayElementType
            );

            throw new InvalidArgumentException($errorMessage);
        }

        if (!str_starts_with($abstractArrayElementType, "\\")) {
            $abstractArrayElementType = "\\$abstractArrayElementType";
        }

        $this->abstractArrayElementType = $abstractArrayElementType;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): mixed
    {
        if (!is_array($value)) {
            $message = sprintf(
                "The provided value '%s' is not an array.",
                print_r($value, true)
            );

            throw new InvalidArgumentException($message);
        }

        foreach ($value as $index => &$element) {
            if ($this->getType($index) !== $this->arrayIndexType) {
                $message = sprintf(
                    "Only keys of type '%s' are allowed for this array.",
                    $this->arrayIndexType
                );

                throw new InvalidArgumentException($message);
            }

            if ($element instanceof $this->abstractArrayElementType) {
                continue;
            }

            // The element might be an array representation of the abstract object.
            if (is_array($element)) {
                $mappedAbstractElement =
                    eval(
                        sprintf(
                            'return new class () extends %s {};',
                            $this->abstractArrayElementType
                        )
                    );
                $dataMappingService->mapArrayOntoObject($element, $mappedAbstractElement);
                $element = $mappedAbstractElement;
                continue;
            }

            $message = sprintf(
                "The provided element '%s' is not of type '%s' "
                . 'or an array representation of that type.',
                print_r($element, true),
                $this->abstractArrayElementType
            );

            throw new InvalidArgumentException($message);
        }

        return $value;
    }
}
