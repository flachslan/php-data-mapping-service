<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Collection;

/**
 * Collection of source naming conventions for validation in the DataMappingService.
 *
 * @package Oktavlachs\DataMappingService\Collection
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class SourceNamingConventions
{
    /**
     * The property name in the source has to match the
     * property name declared in the target object.
     */
    public const TARGET_ONLY = 1;

    /**
     * Besides the property name declared in the target object,
     * also allow source property names in lower_snake_case.
     */
    public const LOWER_SNAKE_CASE = 2;

    /**
     * Besides the property name declared in the target object,
     * also allow source property names in camelCase.
     */
    public const CAMEL_CASE = 4;

    /**
     * Besides the property name declared in the target object,
     * also allow source property names in kebap-case.
     */
    public const KEBAP_CASE = 8;

    /**
     * Besides the property name declared in the target object,
     * also allow source property names in PascalCase.
     */
    public const PASCAL_CASE = 16;

    /**
     * Besides the property name declared in the target object,
     * also allow source property names in UPPER_SNAKE_CASE.
     */
    public const UPPER_SNAKE_CASE = 32;

    /**
     * Check, if the given naming convention options are valid.
     *
     * @param int $conventions The conventions to check
     *
     * @return bool Flag, indicating if the options are valid or not.
     */
    public static function areValidConventions(int $conventions): bool
    {
        return ($conventions < 64) && ($conventions > 0);
    }
}
