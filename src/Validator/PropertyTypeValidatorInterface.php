<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;

/**
 * Validate values of a specific type.
 *
 * @package Oktavlachs\DataMappingService\Validator
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
interface PropertyTypeValidatorInterface
{
    /**
     * Validate a value by the rules of the specific PropertyTypeValidator.
     *
     * @param DataMappingService $dataMappingService The service instance that
     * must be used for validation of nested arrays and array representation
     * of objects
     * @param mixed $value The value under validation
     *
     * @return mixed The validated value
     *
     * @throws InvalidArgumentException If the validation fails
     */
    public function validate(DataMappingService $dataMappingService, mixed $value): mixed;
}
