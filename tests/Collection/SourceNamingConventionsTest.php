<?php

/**  @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Collection;

use PHPUnit\Framework\TestCase;
use Oktavlachs\DataMappingService\Collection\SourceNamingConventions;

/**
 * Class SourceNamingConventionsTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Collection
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class SourceNamingConventionsTest extends TestCase
{
    public function testAreValidConventions(): void
    {
        $this->assertTrue(SourceNamingConventions::areValidConventions(4));
        $this->assertFalse(SourceNamingConventions::areValidConventions(-23525));
    }
}
