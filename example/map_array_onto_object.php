<?php

/**
 * @copyright 2020 Lukas Stermann
 *
 * This script demonstrates how mapping from an array onto an object works
 * with the PHP Data Mapping Service.
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 *
 * @noinspection ForgottenDebugOutputInspection var_dump() calls on purpose
 * to show the state of the target object.
 */

declare(strict_types=1);

use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Exception\DataMappingServiceException;
use Oktavlachs\DataMappingService\Tests\Dummy\Class1;
use Oktavlachs\DataMappingService\Collection\SourceNamingConventions;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/ExampleTargetClass.php';

// Configure your DataMappingService instance.
$dataMappingService = new DataMappingService(
    SourceNamingConventions::LOWER_SNAKE_CASE | SourceNamingConventions::CAMEL_CASE
);

// There are configuration options in the DataMappingService class
$dataMappingService->setIsUsingTargetObjectSetters(true);

$sourceArray = [
    // Uninitialized properties have to be provided in the source
    'uninitialized' => new stdClass(),

    /**
     * This only works because we have set the option in the service
     * to also look different naming conventions in the source.
     *
     * @see DataMappingService::setAllowedSourceNamingConventions()
     */
    'nullable_and_uninitialized' => null,

    /**
     * This property will will use a setter method in the target object
     * class (if defined).
     * @see DataMappingService::setIsUsingTargetObjectSetters()
     */
    'nullableWithSetterMethod' => 100,

    /**
     * The service will only allow elements that are
     * of the type declared for the property in MyExampleClass
     */
    'uninitializedScalarTypeArray' => [
        0 => ['hasStringKeys' => 22, 'and' => 33, 'integerValues' => 44]
    ],

    /**
     * The service doesn't need full namespace names for specific objects
     * in array annotations. It uses the import statements to verify the correct type.
     * This makes your code look a lot cleaner. You can even use aliases!
     * The service also accepts an array representation of a specific object and
     * transforms it into the right type automatically.
     */
    'arrayWithTypesFromUseStatements' => [
        // Actual type
        new Class1(),
        // Array representation of Class1
        [
            'someString' => 'someValue',
            'scalarNestedArray' => [['someIndex' => false, 'anotherIndex' => true]],
            'nullableNestedArray' => null
        ]
    ],

    /**
     * stdClass object types can also be declared as
     * containers for dynamically defined objects.
     */
    'stdClassAsArray' => [
        'firstProperty' => 'firstValue',
        'secondProperty' => 'secondValue'
    ],

    /**
     * Sometimes you must expect mixed types. You should make sure,
     * that this is the last option, because it effectively disables the
     * validation of this type safe oriented service.
     */
    'arrayWithMixedType' => ['literallyAnything', 34, 5.8, true, new stdClass()],

    /**
     * mixed type is supported since PHP 8
     */
    'mixedType' => new class () {}
];

/**
 * The first time the service handles a target object type, it stores all necessary
 * information about the target object in a cache. This boosts performance a lot for
 * future mapping on this type of object.
 *
 * See in the output below how much difference that makes.
 */

$start = 0.0;
$stop = 0.0;

$targetObject = new ExampleTargetClass();
echo "\n======== The target object before mapping ========\n\n";
var_dump($targetObject);

// First instance
$start = microtime(true);
try {
    $dataMappingService->mapArrayOntoObject($sourceArray, $targetObject);
} catch (DataMappingServiceException $e) {
    printf("%s\n", $e->getMessage());
    return;
}
$stop = microtime(true);
$endTimeFirstInstanceInMilliSeconds = round(($stop - $start) * 1000, 4);

echo "\n======== The target object after mapping ========\n\n";
var_dump($targetObject);

// Second instance
$targetObject = new ExampleTargetClass();
$start = microtime(true);
try {
    $dataMappingService->mapArrayOntoObject($sourceArray, $targetObject);
} catch (DataMappingServiceException $e) {
    printf("%s\n", $e->getMessage());
    return;
}

$stop = microtime(true);
$endTimeSecondInstanceInMilliseconds = round(($stop - $start) * 1000, 4);

// Some workload
$targetObject = new ExampleTargetClass();
$start = microtime(true);
for ($i = 0, $iMax = 10_000; $i < $iMax; $i++) {
    try {
        $dataMappingService->mapArrayOntoObject($sourceArray, $targetObject);
    } catch (DataMappingServiceException $e) {
        printf("%s\n", $e->getMessage());
        return;
    }
}
$stop = microtime(true);
$endTimeWorkloadInMilliseconds = round(($stop - $start) * 1000, 4);

printf(
    "\n======== Performance results ========\n\n"
    . "First mapping without cache took %s milliseconds.\n"
    . "Second mapping with cache took %s milliseconds.\n"
    . "The second and all future mappings are therefore -> %s <- times faster.\n"
    . "For another %s mappings the service took %s milliseconds\n",
    $endTimeFirstInstanceInMilliSeconds,
    $endTimeSecondInstanceInMilliseconds,
    round($endTimeFirstInstanceInMilliSeconds / $endTimeSecondInstanceInMilliseconds),
    number_format($iMax),
    $endTimeWorkloadInMilliseconds
);
