<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\ArrayType\StdClassObject;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * Class StdClassObjectTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class StdClassObjectTest extends TestCase
{
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            StdClassObject::class,
            new StdClassObject('int')
        );
    }

    public function testConstructThrowsExceptionOnInvalidIndexType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new StdClassObject(
            'stdClass'
        );
    }

    public function testValidateWithValidObjects(): void
    {
        $validator = new StdClassObject('int');

        $expected = [new stdClass(), new stdClass()];

        $this->assertEquals(
            $expected,
            $validator->validate($this->dataMappingService, $expected)
        );
    }

    public function testValidateWithValidObjectsInArrayRepresentation(): void
    {
        $validator = new StdClassObject('int');

        $classWithStdClassObjectAsArray = [
            'someArray' => [new stdClass(), ['wurst' => 'brot']],
            'someStdClass' => ['haha' => 0]
        ];

        $collectionOfClass2Elements = [
            $classWithStdClassObjectAsArray,
            $classWithStdClassObjectAsArray,
            $classWithStdClassObjectAsArray
        ];

        foreach (
            $validator->validate(
                $this->dataMappingService,
                $collectionOfClass2Elements
            ) as $result
        ) {
            $this->assertInstanceOf(stdClass::class, $result);
        }
    }

    public function testValidateThrowsExceptionOnWrongValueElementType(): void
    {
        $validator = new StdClassObject('int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, [3.3]);
    }

    public function testValidateThrowsExceptionOnWrongValueType(): void
    {
        $validator = new StdClassObject('int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, 3);
    }

    public function testValidateThrowsExceptionOnWrongValueIndexType(): void
    {
        $validator = new StdClassObject('int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, ['someStringIndex' => 3]);
    }
}
