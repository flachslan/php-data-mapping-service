<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\SimpleType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\AbstractPropertyTypeValidator;

/**
 * A validator for scalar type values.
 *
 * @package Oktavlachs\DataMappingService\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class Scalar extends AbstractPropertyTypeValidator
{
    private string $scalarType;

    /**
     * Scalar constructor.
     *
     * @param string $scalarType The scalar type that must be validated.
     */
    public function __construct(string $scalarType)
    {
        if (!$this->representsScalarType($scalarType)) {
            $message = sprintf(
                "The type '%s' is not a scalar type. "
                . 'Please use other validators for array validation.',
                $scalarType
            );

            throw new InvalidArgumentException($message);
        }

        $this->scalarType = $scalarType;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): mixed
    {
        if ($this->getType($value) !== $this->scalarType) {
            $message = sprintf(
                "The provided value '%s' is not of type '%s'.",
                print_r($value, true),
                $this->scalarType
            );

            throw new InvalidArgumentException($message);
        }

        return $value;
    }
}
