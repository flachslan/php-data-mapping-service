<?php

/**
 * @copyright 2020 Lukas Stermann
 *
 * @noinspection PhpUnusedPrivateFieldInspection In this example
 * file there are no setters/getters implemented
 */

declare(strict_types=1);

use Oktavlachs\DataMappingService\Tests\Dummy\Class1 as Class1Alias;

/**
 * This class sums up how you can define a class that shall
 * be used as a target object for the data mapping service.
 *
 * Your class doesn't depend on setters for protected/private
 * properties. The values are set by reflection anyway
 * without changing the access modifiers in your objects.
 *
 * Of course the service is not limited to the types of
 * properties used in this example.
 * You may use all kind of scalar types, arrays, specific
 * objects, ...
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class ExampleTargetClass
{
    private stdClass $uninitialized;

    public float $initialized = 42.0;

    private ?bool $nullableAndUninitialized;

    private ?int $nullableWithSetterMethod;

    /**
     * Optionally use a setter method to manipulate data before it is set in the object.
     *
     * @see \Oktavlachs\DataMappingService\DataMappingService::setIsUsingTargetObjectSetters()
     *
     * @param int|null $int The value for the property after
     * it was validated by the DataMappingService.
     *
     * @return void
     */
    public function setNullableWithSetterMethod(?int $int): void
    {
        if ($int === null) {
            $this->nullableWithSetterMethod = $int;
        } else {
            $this->nullableWithSetterMethod = $int - 1;
        }
    }

    protected ?ExampleTargetClass $nullableAndInitializedWithNull = null;

    private ?string $nullableAndInitializedWithType = 'defaultString';

    private stdClass $stdClassAsArray;

    /**
     * @var array<int, array<string, int>>
     */
    private array $uninitializedScalarTypeArray;

    /**
     * @var array<int, Class1Alias>
     */
    private array $arrayWithTypesFromUseStatements;

    /**
     * @var array<int, mixed>
     */
    private array $arrayWithMixedType;

    /**
     * @var mixed
     */
    private mixed $mixedType;
}
