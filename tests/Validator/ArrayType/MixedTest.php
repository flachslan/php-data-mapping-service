<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\ArrayType\MixedValidator;
use PHPUnit\Framework\TestCase;

/**
 * Class MixedTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class MixedTest extends TestCase
{
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            MixedValidator::class,
            new MixedValidator('int')
        );
    }

    public function testConstructThrowsExceptionOnInvalidIndexType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new MixedValidator('stdClass');
    }

    public function testValidateWithValidObjects(): void
    {
        $validator = new MixedValidator('int');

        $expected = [3, 'anything'];

        $this->assertEquals($expected, $validator->validate($this->dataMappingService, $expected));
    }

    public function testValidateThrowsExceptionOnWrongValueType(): void
    {
        $validator = new MixedValidator('int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, 3);
    }

    public function testValidateThrowsExceptionOnWrongValueIndexType(): void
    {
        $validator = new MixedValidator('int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, ['someStringIndex' => 3]);
    }
}
