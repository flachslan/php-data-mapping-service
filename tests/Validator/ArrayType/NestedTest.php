<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\ArrayType\Nested;
use Oktavlachs\DataMappingService\Validator\PropertyTypeValidatorInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Class NestedTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class NestedTest extends TestCase
{
    private DataMappingService $dataMappingService;

    /**
     * @var PropertyTypeValidatorInterface|MockObject
     */
    private $nextLevelArrayValidator;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
        $this->nextLevelArrayValidator =
            $this->createMock(PropertyTypeValidatorInterface::class);
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            Nested::class,
            new Nested('int', $this->nextLevelArrayValidator)
        );
    }

    public function testConstructThrowsExceptionOnInvalidIndexType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Nested('sdtClass', $this->nextLevelArrayValidator);
    }

    public function testValidate(): void
    {
        $this->nextLevelArrayValidator->method('validate')->willReturn(3.3);

        $validator = new Nested(
            'int',
            $this->nextLevelArrayValidator
        );
        $this->assertSame([3.3], $validator->validate($this->dataMappingService, [3.3]));
    }

    public function testValidateThrowsExceptionOnWrongValueType(): void
    {
        $validator = new Nested(
            'int',
            $this->nextLevelArrayValidator
        );

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, 3);
    }

    public function testValidateThrowsExceptionOnWrongValueIndexType(): void
    {
        $validator = new Nested(
            'int',
            $this->nextLevelArrayValidator
        );

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, ['someStringIndex' => 3]);
    }
}
