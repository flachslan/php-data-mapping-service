<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\SimpleType;

use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\PropertyTypeValidatorInterface;

/**
 * A validator for mixed type values.
 *
 * @package Oktavlachs\DataMappingService\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class MixedValidator implements PropertyTypeValidatorInterface
{
    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): mixed
    {
        return $value;
    }
}
