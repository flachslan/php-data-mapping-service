<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Reflector;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\Reflector\ExtendedReflectionClass;
use Oktavlachs\DataMappingService\Tests\Dummy\UseStatements\{
    ClassWithAllKindsOfDifferentUseStatements,
    ClassWithoutUseStatements,
    SomeParentUseStatementClass};
use Oktavlachs\DataMappingService\Tests\Dummy\{Class1, Class2};
use PHPUnit\Framework\TestCase;
use ReflectionException;
use stdClass;

/**
 * Class ExtendedReflectionClassTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Reflector
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class ExtendedReflectionClassTest extends TestCase
{
    private ExtendedReflectionClass $extendedReflectionObject;

    public function setUp(): void
    {
        try {
            $this->extendedReflectionObject = new ExtendedReflectionClass(
                new ClassWithAllKindsOfDifferentUseStatements()
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            ExtendedReflectionClass::class,
            $this->extendedReflectionObject
        );
    }

    public function testConstructFailsOnNoneUserDefinedObject(): void
    {
        $this->expectException(InvalidArgumentException::class);
        try {
            new ExtendedReflectionClass(new stdClass());
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testGetUseStatement(): void
    {
        $class2 = $this->extendedReflectionObject->getUseStatement('Class2');
        $classOne = $this->extendedReflectionObject->getUseStatement('ClassOne');
        $parentClass = $this->extendedReflectionObject->getUseStatement('ParentClass');
        $classWithoutUseStatements =
            $this->extendedReflectionObject->getUseStatement('ClassWithoutUseStatements');
        $stdClass = $this->extendedReflectionObject->getUseStatement('stdClass');

        $this->assertSame("\\" . Class2::class, $class2);
        $this->assertSame("\\" . Class1::class, $classOne);
        $this->assertSame("\\" . SomeParentUseStatementClass::class, $parentClass);
        $this->assertSame(
            "\\" . ClassWithoutUseStatements::class,
            $classWithoutUseStatements
        );
        $this->assertSame("\\" . stdClass::class, $stdClass);
    }

    public function testGetNonExistentUseStatement(): void
    {
        $this->assertNull(
            $this->extendedReflectionObject->getUseStatement('someClassThatDoesNotExist')
        );
    }

    public function testGetUseStatementFromClassWithoutUseStatements(): void
    {
        try {
            $this->extendedReflectionObject =
                new ExtendedReflectionClass(new ClassWithoutUseStatements());
        } catch (ReflectionException $e) {
            // Never thrown.
        }

        $this->assertNull(
            $this->extendedReflectionObject->getUseStatement(
                'thisClassHasNoUseStatements'
            )
        );
    }
}
