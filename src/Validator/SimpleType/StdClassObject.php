<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\SimpleType;

use Oktavlachs\DataMappingService\DataMappingService;
use stdClass;
use InvalidArgumentException;
use Oktavlachs\DataMappingService\Validator\PropertyTypeValidatorInterface;

/**
 * A validator for stdClass object type values.
 *
 * @package Oktavlachs\DataMappingService\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class StdClassObject implements PropertyTypeValidatorInterface
{
    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): stdClass
    {
        if ($value instanceof stdClass) {
            return $value;
        }

        if (is_array($value)) {
            return (object) $value;
        }

        $message = sprintf(
            "The provided value '%s' is not of type "
            . "'stdClass' or an array representation of that type.",
            print_r($value, true)
        );

        throw new InvalidArgumentException($message);
    }
}
