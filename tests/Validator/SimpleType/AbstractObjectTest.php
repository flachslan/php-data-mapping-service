<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\SimpleType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Tests\Dummy\{Class1,
    Class2,
    UseStatements\AbstractClass};
use Oktavlachs\DataMappingService\Validator\SimpleType\AbstractObject;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractObjectTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class AbstractObjectTest extends TestCase
{
    /**
     * @var DataMappingService|MockObject
     */
    private $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            AbstractObject::class,
            new AbstractObject(AbstractClass::class)
        );
    }

    public function testConstructFailsOnInvalidObjectType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new AbstractObject('array');
    }

    public function testConstructDoesntAllowSpecificObjectTypes(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new AbstractObject(Class2::class);
    }

    public function testValidate(): void
    {
        $validator = new AbstractObject(AbstractClass::class);

        $this->assertInstanceOf(
            AbstractClass::class,
            $validator->validate(
                $this->dataMappingService,
                new class () extends AbstractClass {
                }
            )
        );

        $this->assertInstanceOf(
            AbstractClass::class,
            $validator->validate(
                $this->dataMappingService,
                ['someString' => 'string']
            )
        );
    }

    public function testValidateWithWrongValue(): void
    {
        $validator = new AbstractObject(AbstractClass::class);

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, new Class1());
    }
}
