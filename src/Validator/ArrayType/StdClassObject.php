<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\AbstractPropertyTypeValidator;
use stdClass;

/**
 * A validator for array stdClass object type values.
 *
 * @package Oktavlachs\DataMappingService
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class StdClassObject extends AbstractPropertyTypeValidator
{
    private string $arrayIndexType;

    /**
     * StdClassObject constructor.
     *
     * @param string $arrayIndexType The type that each index of the array
     * must be of (probably int or string)
     */
    public function __construct(string $arrayIndexType)
    {
        $this->validateArrayIndexType($arrayIndexType);
        $this->arrayIndexType = $arrayIndexType;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): array
    {
        if (!is_array($value)) {
            $message = sprintf(
                "The provided value '%s' is not an array.",
                print_r($value, true)
            );

            throw new InvalidArgumentException($message);
        }

        foreach ($value as $index => &$element) {
            if ($this->getType($index) !== $this->arrayIndexType) {
                $message = sprintf(
                    "Only keys of type '%s' are allowed for this array.",
                    $this->arrayIndexType
                );

                throw new InvalidArgumentException($message);
            }

            if ($element instanceof stdClass) {
                continue;
            }

            if (is_array($element)) {
                $element = (object) $element;
                continue;
            }

            $message = sprintf(
                "The provided value '%s' is not of type "
                . "'stdClass' or an array representation of that type.",
                print_r($value, true)
            );

            throw new InvalidArgumentException($message);
        }

        return $value;
    }
}
