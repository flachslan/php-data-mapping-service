<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Exception;

use Exception;

/**
 * Wraps exceptions which are thrown in the DataMappingService.
 *
 * @package Oktavlachs\DataMappingService\Exception
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
abstract class DataMappingServiceException extends Exception
{
}
