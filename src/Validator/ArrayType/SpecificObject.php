<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\ArrayType;

use ReflectionClass;
use ReflectionException;
use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\AbstractPropertyTypeValidator;

/**
 * A validator for array with elements of a specific object type.
 *
 * @package Oktavlachs\DataMappingService\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class SpecificObject extends AbstractPropertyTypeValidator
{
    private string $arrayIndexType;

    private string $arrayElementType;

    /**
     * SpecificObject constructor.
     *
     * @param string $arrayIndexType The type that each index of the array
     * must be of (probably int or string)
     * @param string $arrayElementType The type that each element of the array
     * must be of
     */
    public function __construct(
        string $arrayIndexType,
        string $arrayElementType
    ) {
        $this->validateArrayIndexType($arrayIndexType);
        $this->arrayIndexType = $arrayIndexType;

        try {
            $abstractArrayElementClass = new ReflectionClass($arrayElementType);
        } catch (ReflectionException $e) {
            $errorMessage = sprintf(
                "arrayElementType '%s' is not supported.",
                $arrayElementType
            );

            throw new InvalidArgumentException($errorMessage, $e->getCode(), $e);
        }

        if ($abstractArrayElementClass->isAbstract()) {
            $errorMessage = sprintf(
                'Abstract classes are not supported as array elements. '
                . "Given array element type: '%s'.",
                $arrayElementType
            );

            throw new InvalidArgumentException($errorMessage);
        }

        if (!str_starts_with($arrayElementType, "\\")) {
            $arrayElementType = "\\$arrayElementType";
        }

        $this->arrayElementType = $arrayElementType;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): array
    {
        if (!is_array($value)) {
            $message = sprintf(
                "The provided value '%s' is not an array.",
                print_r($value, true)
            );

            throw new InvalidArgumentException($message);
        }

        foreach ($value as $index => &$element) {
            if ($this->getType($index) !== $this->arrayIndexType) {
                $message = sprintf(
                    "Only keys of type '%s' are allowed for this array.",
                    $this->arrayIndexType
                );

                throw new InvalidArgumentException($message);
            }

            if ($element instanceof $this->arrayElementType) {
                continue;
            }

            // The element might be an array representation of the specific object.
            if (is_array($element)) {
                $mappedArrayElement = new $this->arrayElementType();
                $dataMappingService->mapArrayOntoObject($element, $mappedArrayElement);
                $element = $mappedArrayElement;
                continue;
            }

            $message = sprintf(
                "The provided element '%s' is not of type '%s' "
                . 'or an array representation of that type.',
                print_r($element, true),
                $this->arrayElementType
            );

            throw new InvalidArgumentException($message);
        }

        return $value;
    }
}
