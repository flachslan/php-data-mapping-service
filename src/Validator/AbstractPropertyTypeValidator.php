<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator;

use InvalidArgumentException;

/**
 * Abstraction layer for validation methods in property type validator implementations.
 *
 * @package Oktavlachs\DataMappingService\Validator
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
abstract class AbstractPropertyTypeValidator implements PropertyTypeValidatorInterface
{
    protected function validateArrayIndexType(string $arrayIndexType): void
    {
        switch ($arrayIndexType) {
            case 'int':
            case 'string':
                return;
            default:
                $errorMessage = sprintf(
                    "The index type must be string or int. Actual value was '%s'",
                    $arrayIndexType
                );

                throw new InvalidArgumentException($errorMessage);
        }
    }

    /**
     * Get the type of a variable.
     *
     * Return the new and commonly used string representations of types in PHP.
     * The results differ from to the result of the built in 'gettype()' function,
     * as this one gives back type names that are backwards compatible to very
     * early PHP versions.
     *
     * @param mixed $value Any value you want to get the type of
     *
     * @return string The newer type name of the given variable
     */
    protected function getType(mixed $value): string
    {
        $type = gettype($value);

        return match ($type) {
            'integer' => 'int',
            'boolean' => 'bool',
            'double' => 'float',
            'NULL' => 'null',
            default => $type,
        };
    }

    /**
     * Check if a string value represents a scalar PHP type.
     *
     * @param string $string The string to check on
     *
     * @return bool Flag which show if the string represents a scalar type
     */
    protected function representsScalarType(string $string): bool
    {
        return match ($string) {
            'string', 'int', 'float', 'bool' => true,
            default => false,
        };
    }
}
