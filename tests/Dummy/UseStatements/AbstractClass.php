<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Dummy\UseStatements;

/**
 * Class AbstractClass
 *
 * @package Oktavlachs\DataMappingService\Tests\Dummy\UseStatements
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
abstract class AbstractClass
{
    public string $someString;
}
