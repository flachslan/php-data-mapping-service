<?php

/**  @copyright 2019 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Dummy;

/**
 * Class Class1
 *
 * @package Oktavlachs\DataMappingService\Tests\Dummy
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class Class1
{
    public string $someString;

    /**
     * @var array<int, array<string, bool>>
     */
    public array $scalarNestedArray;

    /**
     * @var array<int, array<string, float>>
     */
    public ?array $nullableNestedArray;
}
