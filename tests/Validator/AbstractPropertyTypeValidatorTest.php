<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\AbstractPropertyTypeValidator;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * Class AbstractPropertyTypeValidatorTest
 *
 * @package Oktavlachs\DataMappingService\Tests
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class AbstractPropertyTypeValidatorTest extends TestCase
{
    private AbstractPropertyTypeValidator $abstractTypeValidator;

    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();

        $this->abstractTypeValidator = new class (
        ) extends AbstractPropertyTypeValidator {
            /**
             * @inheritDoc
             */
            public function validate($dataMappingService, $value): mixed
            {
                // Only in child classes
            }
        };
    }

    public function testCreate(): void
    {
        $this->assertInstanceOf(
            AbstractPropertyTypeValidator::class,
            $this->abstractTypeValidator
        );
    }

    public function testRepresentsScalarType(): void
    {
        $concreteTypeValidator = new class () extends AbstractPropertyTypeValidator {
            /**
             * @inheritDoc
             */
            public function validate($dataMappingService, $value): mixed
            {
            }

            public function representsScalarType(string $string): bool
            {
                return parent::representsScalarType(
                    $string
                );
            }
        };
        $this->assertTrue($concreteTypeValidator->representsScalarType('string'));
        $this->assertFalse($concreteTypeValidator->representsScalarType('wurst'));
    }

    public function testGetType(): void
    {
        $concreteTypeValidator = new class () extends AbstractPropertyTypeValidator {
            /**
             * @inheritDoc
             */
            public function validate($dataMappingService, $value): mixed
            {
            }

            public function getType($value): string
            {
                return parent::getType($value);
            }
        };
        $this->assertSame('int', $concreteTypeValidator->getType(2));
        $this->assertSame('bool', $concreteTypeValidator->getType(false));
        $this->assertSame('float', $concreteTypeValidator->getType(3.3));
        $this->assertSame('null', $concreteTypeValidator->getType(null));
        $this->assertSame('object', $concreteTypeValidator->getType(new stdClass()));
        $this->assertSame('string', $concreteTypeValidator->getType('someString'));
        $this->assertSame(
            'object',
            $concreteTypeValidator->getType(
                static function () {
                }
            )
        );
    }

    public function testRepresentsArrayIndexType(): void
    {
        $concreteTypeValidator = new class () extends AbstractPropertyTypeValidator {
            /**
             * @inheritDoc
             */
            public function validate($dataMappingService, $value): mixed
            {
            }

            public function validateArrayIndexType(string $string): void
            {
                parent::validateArrayIndexType(
                    $string
                );
            }
        };

        $concreteTypeValidator->validateArrayIndexType('string');
        $concreteTypeValidator->validateArrayIndexType('int');

        $this->expectException(InvalidArgumentException::class);
        $concreteTypeValidator->validateArrayIndexType('float');
    }
}
