# PHP Data Mapping Service

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=Oktavlachs_php-data-mapping-service&metric=security_rating)](https://sonarcloud.io/dashboard?id=Oktavlachs_php-data-mapping-service)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=Oktavlachs_php-data-mapping-service&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=Oktavlachs_php-data-mapping-service)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=Oktavlachs_php-data-mapping-service&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=Oktavlachs_php-data-mapping-service)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=Oktavlachs_php-data-mapping-service&metric=coverage)](https://sonarcloud.io/dashboard?id=Oktavlachs_php-data-mapping-service)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=Oktavlachs_php-data-mapping-service&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=Oktavlachs_php-data-mapping-service)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=Oktavlachs_php-data-mapping-service&metric=sqale_index)](https://sonarcloud.io/dashboard?id=Oktavlachs_php-data-mapping-service)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=Oktavlachs_php-data-mapping-service&metric=ncloc)](https://sonarcloud.io/dashboard?id=Oktavlachs_php-data-mapping-service)

![Licence](https://img.shields.io/badge/licence-MIT-blue.svg)

The PHP Data Mapping Service type safely maps raw source data onto target objects in a >=
PHP 7.4 way: Your target object defines the way it expects source data to match - no need
for type validation anymore. On top of that there is almost no use for annotation type
hinting anymore.

Supported raw data types: array, stdClass, JSON string, XML string, SimpleXMLElement

supported target object types: mixed, scalar types (string, bool, int, float),
user defined objects, stdClass, abstract classes, 

arrays:
This has to be specified in the target properties doc commen.

Please use the syntax: `@var array<arrayKeyType, arrayElementType>`

Allowed arrayKeyType -> 'string' or 'int'.

Allowed arrayElementType -> nested array, user specific objects

user specific objects in arrays:
You don't have to provide the full namespace. It is fine if you use a use statement for
this or (if the user object is in the same namespace as the target objects class), simply
provide the short name.

This service is designed with different users in mind (exception handling)... TODO

## Table of Contents

* [Installation](#installation)
  * [Dependencies](#dependencies)
  * [Stable Release](#stable-release)
  * [Development Version](#development-version)
* [Usage](#usage)
* [Mappable data types](#mappable-data-types)
* [Contributors](#contributors)
* [License](#license)


## Installation

### Dependencies

### Stable Release

### Development Version

## Usage

## Mappable source data types

This mapping service supports basic data types most of your use cases boil down to:

Source
* array
* stdClass
* json (string)
* xml (string)
* SimpleXmlElement

TODO: The outstanding behaviour with initialized null by mixed types !!!!!!!!!!!!!!!
### CLI

## Contributors

### Maintainer

* [Lukas Stermann](https://gitlab.com/Oktavlachs)

### Others

* [Matthias Bräuer](https://gitlab.com/Braeuer) (Testing and Code Reviews)
* [Rick Barenthin](https://github.com/dunst0) (Testing and Code Reviews)
* [Benedikt Vollmerhaus](https://gitlab.com/BVollmerhaus) (Testing)

## License

PHP Data Mapping Service is under the MIT license. See
[LICENSE](https://gitlab.com/Oktavlachs/php-data-mapping-service/blob/master/LICENSE)
for more information.
