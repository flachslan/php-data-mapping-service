<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\SimpleType;

use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\SimpleType\MixedValidator;
use PHPUnit\Framework\TestCase;

/**
 * Class MixedTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class MixedTest extends TestCase
{
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(MixedValidator::class, new MixedValidator());
    }

    public function testValidate(): void
    {
        $validator = new MixedValidator();

        $this->assertSame(
            'literallyAnything',
            $validator->validate($this->dataMappingService, 'literallyAnything')
        );
    }
}
