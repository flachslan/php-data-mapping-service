<?php

/**  @copyright 2019 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Dummy;

use Oktavlachs\DataMappingService\Tests\Dummy\Class1 as ParentClass;
use Oktavlachs\DataMappingService\Tests\Dummy\UseStatements\ClassWithStdClassObject;

/**
 * Class Class2
 *
 * @package Oktavlachs\DataMappingService\Tests\Dummy
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 *
 * TODO: Test, that is mapping parent class on variables in subclass?!
 *  -> also get those import statements
 * TODO: add missing use statements for classes within the same namespace
 *  on the fly while validating the target class
 */
final class Class2 extends Class1
{
    public ParentClass $userDefinedObjectAsAlias;

    /**
     * @var array<int, array<int, ClassWithStdClassObject>>
     */
    public array $arrayWithObjectsWithoutFullNamespace;

    /**
     * @var array<int, array<int, ParentClass>>
     */
    public array $arrayWithObjectsAsAliases;

    /**
     * @var mixed
     */
    public mixed $mixedTypeAlwaysInitializedWithNull = null;
}
