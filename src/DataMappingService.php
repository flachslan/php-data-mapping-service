<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService;

use stdClass;
use JsonException;
use SimpleXMLElement;
use InvalidArgumentException;
use Oktavlachs\DataMappingService\Cache\TargetObjectPropertyInformation
    as TargetObjectPropertyInformationCache;
use Oktavlachs\DataMappingService\Exception\{SourcePropertyValidationException,
    TargetPropertyInspectionException};
use Oktavlachs\DataMappingService\Collection\{CachedProperty, SourceNamingConventions};

/**
 * A service which maps simple data structures onto objects.
 *
 * @package Oktavlachs\DataMappingService
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class DataMappingService
{
    /**
     * @see DataMappingService::setIsUsingTargetObjectSetters()
     */
    private bool $isUsingTargetObjectSetters = true;

    /**
     * Information about the allowed source property naming conventions.
     *
     * In case the declared name of the target property doesn't exist in the
     * source property, the service will search for source properties
     * that match a different naming convention, if it is allowed to.
     * If all naming conventions are allowed, the first match will win.
     *
     * @see https://www.php-fig.org/psr/psr-1/#42-properties For target conventions
     * @see SourceNamingConventions For source conventions
     *
     * @var int $sourceNamingConventions Bitmask, which indicates what kind of source
     * property naming conventions are enabled. You can combine different
     * conventions with the | operator.
     */
    private int $sourceNamingConventions;

    private TargetObjectPropertyInformationCache $targetObjectPropertyInformationCache;

    /**
     * Set the using target object setters option.
     *
     * When set to true, the service will use setter functions from the
     * target object class to set a source value in the target object.
     * This is helpful when you want to manipulate source data for the
     * target object after type validation by the DataMappingService.
     * If no setter function is defined in the target object class,
     * the source value will be set in the target object property through
     * reflection, ignoring the properties modifier.
     *
     * When set to false, the source values will always be set in the
     * target object through reflection, ignoring the properties modifier.
     *
     * @param bool $isUsingTargetObjectSetters Flag, indicating
     * weather the using target object setters option is set.
     *
     * @return void
     */
    public function setIsUsingTargetObjectSetters(bool $isUsingTargetObjectSetters): void
    {
        $this->isUsingTargetObjectSetters = $isUsingTargetObjectSetters;
    }

    /**
     * DataMappingService constructor.
     *
     * @param int $sourceNamingConventions The allowed naming
     * conventions for source property validation. See $
     */
    public function __construct(
        int $sourceNamingConventions = SourceNamingConventions::TARGET_ONLY
    ) {
        if (!SourceNamingConventions::areValidConventions($sourceNamingConventions)) {
            $errorMessage = sprintf(
                "The conventions '%s' are not a combination of valid naming "
                . "conventions. Have a look at collection '%s'",
                $sourceNamingConventions,
                SourceNamingConventions::class
            );

            throw new InvalidArgumentException($errorMessage);
        }

        $this->sourceNamingConventions = $sourceNamingConventions;

        $this->targetObjectPropertyInformationCache =
            TargetObjectPropertyInformationCache::getInstance();
    }

    /**
     * Map a SimpleXMLElement onto a target object instance.
     *
     * @param SimpleXMLElement $simpleXMLElement The source object to map from
     * @param object $targetObject The target object to map onto
     *
     * @throws SourcePropertyValidationException In case the
     * validation of a source property fails
     * @throws TargetPropertyInspectionException In case the
     * inspection of a target property fails
     */
    public function mapSimpleXmlElementOntoObject(
        SimpleXMLElement $simpleXMLElement,
        object $targetObject
    ): void {
        $this->mapArrayOntoObject((array) $simpleXMLElement, $targetObject);
    }

    /**
     * Map a raw XML string source onto a target object instance.
     *
     * @param string $rawXml The raw xml string source
     * @param object $targetObject The target object to map onto
     * @param string $className @see SimpleXMLElement::__construct()
     * @param int $options @see SimpleXMLElement::__construct()
     * @param string $namespace @see SimpleXMLElement::__construct()
     * @param bool $isPrefix @see SimpleXMLElement::__construct()
     *
     * @throws SourcePropertyValidationException In case the
     * validation of a source property fails
     * @throws TargetPropertyInspectionException In case the
     * inspection of a target property fails
     */
    public function mapRawXmlOntoObject(
        string $rawXml,
        object $targetObject,
        string $className = 'SimpleXMLElement',
        int $options = 0,
        string $namespace = '',
        bool $isPrefix = false
    ): void {
        libxml_use_internal_errors(true);

        $simpleXmlElement = simplexml_load_string(
            $rawXml,
            $className,
            $options,
            $namespace,
            $isPrefix
        );

        if ($simpleXmlElement === false) {
            $message = sprintf(
                "XML Error -> '%s'. Raw XML was: '%s'",
                print_r(libxml_get_errors(), true),
                $rawXml
            );

            libxml_clear_errors();

            /*
             * Disable or else there will be side effects on
             * other usage of libxml outside this class
             */
            libxml_use_internal_errors(false);

            throw new SourcePropertyValidationException($message);
        }

        /*
         * Disable or else there will be side effects on
         * other usage of libxml outside this class
         */
        libxml_use_internal_errors(false);

        $this->mapArrayOntoObject((array) $simpleXmlElement, $targetObject);
    }

    /**
     * Map a raw JSON string onto an object.
     *
     * @param string $rawJson The incoming raw JSON as a string
     * @param object $targetObject The object to map onto
     *
     * @throws SourcePropertyValidationException In case the
     * validation of a source property fails
     * @throws TargetPropertyInspectionException In case the
     * inspection of a target property fails
     */
    public function mapRawJsonOntoObject(string $rawJson, object $targetObject): void
    {
        try {
            $this->mapArrayOntoObject(
                json_decode($rawJson, true, 512, JSON_THROW_ON_ERROR),
                $targetObject
            );
        }
        catch (JsonException $e) {
            $message = sprintf(
                "JSON exception: '%s' for provided JSON string -> '%s'",
                $e->getMessage(),
                $rawJson
            );

            throw new SourcePropertyValidationException($message);
        }
    }

    /**
     * Map a stdClass object onto an object.
     *
     * @param stdClass $sourceObject The stdClass object to map from
     * @param object $targetObject The object to map onto
     *
     * @throws SourcePropertyValidationException In case the
     * validation of a source property fails
     * @throws TargetPropertyInspectionException In case the
     * inspection of a target property fails
     */
    public function mapStdClassOntoObject(
        stdClass $sourceObject,
        object $targetObject
    ): void {
        $this->mapArrayOntoObject((array) $sourceObject, $targetObject);
    }

    /**
     * Map an array onto an object.
     *
     * @param array $sourceArray The array to map from
     * @param object $targetObject The object to map onto
     *
     * @throws SourcePropertyValidationException In case the
     * validation of a source property fails
     * @throws TargetPropertyInspectionException In case the
     * inspection of a target property fails
     */
    public function mapArrayOntoObject(array $sourceArray, object $targetObject): void
    {
        $cachedPropertyInformationCollection =
            $this->targetObjectPropertyInformationCache->get($targetObject);

        $validatedProperties = [];

        foreach (
            $cachedPropertyInformationCollection as $propertyName => $propertyInformation
        ) {
            $sourceObjectPropertyValue = $this->getSourcePropertyValue(
                $targetObject,
                $propertyName,
                $propertyInformation,
                $sourceArray
            );

            // Check on nullable types
            if ($sourceObjectPropertyValue === null) {
                if ($propertyInformation[CachedProperty::ALLOWS_NULL]) {
                    $validatedProperties[$propertyName] = $sourceObjectPropertyValue;
                    continue;
                }

                $errorMessage = sprintf(
                    "Source provided 'null', which is not allowed "
                    . "as a value for property '%s' in class '%s'",
                    $propertyName,
                    get_class($targetObject)
                );

                throw new SourcePropertyValidationException($errorMessage);
            }

            try {
                $sourceObjectPropertyValue =
                    $propertyInformation[CachedProperty::VALIDATOR]
                        ->validate($this, $sourceObjectPropertyValue);
            } catch (InvalidArgumentException $e) {
                $wrappingErrorMessage = sprintf(
                    "Source validation error for target property '%s' in class '%s'",
                    $propertyName,
                    get_class($targetObject)
                );

                throw new SourcePropertyValidationException(
                    $wrappingErrorMessage,
                    $e->getCode(),
                    $e
                );
            }

            $validatedProperties[$propertyName] = $sourceObjectPropertyValue;
        }

        /*
         * We assign all values to the target object properties AFTER
         * all source property values have been validated in order to
         * avoid side effects.
         * If we would not do this, the target object remains
         * manipulated for property A (assuming the sources value for
         * A was valid), when property B fails the validation.
         */
        $this->assignValidatedSourceValuesToTargetObjectProperties(
            $validatedProperties,
            $targetObject,
            $cachedPropertyInformationCollection
        );
    }

    /**
     * Get the value of a sources property.
     *
     * @param object $targetObject The target object to map onto.
     * @param string $targetObjectPropertyName The target object properties name
     * @param array<int, mixed> $cachedTargetObjectProperty The cached information
     * about a target object property
     * @param array $sourceArray The source in an array representation
     *
     * @return mixed
     *
     * @throws SourcePropertyValidationException In case the
     * validation of a source property fails
     */
    private function getSourcePropertyValue(
        object $targetObject,
        string $targetObjectPropertyName,
        array $cachedTargetObjectProperty,
        array $sourceArray
    ): mixed {
        /*
         * array_key_exists is used for the following expression
         * because isset() cannot distinguish between an array
         * key associated with null, and an actually unset key.
         *
         * $array = [null, 'myKey' => null]
         * isset($array[0]) -----> false
         * isset($array['myKey']) -----> false
         * isset($array[50]) -----> false
         */
        if (array_key_exists($targetObjectPropertyName, $sourceArray)) {
            return $sourceArray[$targetObjectPropertyName];
        }

        /*
         * The target object property name does not
         * exist in the same naming case convention.
         */
        if ($this->sourceNamingConventions & SourceNamingConventions::TARGET_ONLY) {
            // The value is not in the source but has default in the target object anyway
            if ($cachedTargetObjectProperty[CachedProperty::IS_INITIALIZED]) {
                return $cachedTargetObjectProperty[CachedProperty::REFLECTION_INSTANCE]
                    ->getValue($targetObject);
            }

            $errorMessage = sprintf(
                "The target object property '%s' of class '%s' either doesn't "
                . 'exist in the source or is not defined in the same naming convention '
                . "as the property name in the source '%s'",
                $targetObjectPropertyName,
                /*
                 * Not using declaring class name from cached property information
                 * because that leads to confusion in the error message when using
                 * child classes.
                 */
                get_class($targetObject),
                print_r($sourceArray, true)
            );

            throw new SourcePropertyValidationException($errorMessage);
        }

        // Check for alternative naming conventions in the source
        if (
            ($this->sourceNamingConventions & SourceNamingConventions::LOWER_SNAKE_CASE)
            && array_key_exists(
                $cachedTargetObjectProperty[CachedProperty::NAME_LOWER_SNAKE_CASE],
                $sourceArray
            )
        ) {
            return $sourceArray[
                $cachedTargetObjectProperty[CachedProperty::NAME_LOWER_SNAKE_CASE]
            ];
        }

        if (
            ($this->sourceNamingConventions & SourceNamingConventions::CAMEL_CASE)
            && array_key_exists(
                $cachedTargetObjectProperty[CachedProperty::NAME_CAMEL_CASE],
                $sourceArray
            )
        ) {
            return $sourceArray[
                   $cachedTargetObjectProperty[CachedProperty::NAME_CAMEL_CASE]
            ];
        }

        if (
            ($this->sourceNamingConventions & SourceNamingConventions::KEBAP_CASE)
            && array_key_exists(
                $cachedTargetObjectProperty[CachedProperty::NAME_KEBAP_CASE],
                $sourceArray
            )
        ) {
            return $sourceArray[
                   $cachedTargetObjectProperty[CachedProperty::NAME_KEBAP_CASE]
            ];
        }

        if (
            ($this->sourceNamingConventions & SourceNamingConventions::PASCAL_CASE)
            && array_key_exists(
                $cachedTargetObjectProperty[CachedProperty::NAME_PASCAL_CASE],
                $sourceArray
            )
        ) {
            return $sourceArray[
                   $cachedTargetObjectProperty[CachedProperty::NAME_PASCAL_CASE]
            ];
        }

        if (
            ($this->sourceNamingConventions & SourceNamingConventions::UPPER_SNAKE_CASE)
            && array_key_exists(
                $cachedTargetObjectProperty[CachedProperty::NAME_UPPER_SNAKE_CASE],
                $sourceArray
            )
        ) {
            return $sourceArray[
                   $cachedTargetObjectProperty[CachedProperty::NAME_UPPER_SNAKE_CASE]
            ];
        }

        // The value is not in the source but has default in the target object anyway
        if ($cachedTargetObjectProperty[CachedProperty::IS_INITIALIZED]) {
            return $cachedTargetObjectProperty[CachedProperty::REFLECTION_INSTANCE]
                ->getValue($targetObject);
        }

        $errorMessage = sprintf(
            "The target object property '%s' in class '%s' either doesn't "
            . 'exist in the source or is not defined in an allowed naming convention '
            . "(see class '%s') in the source '%s'.",
            $targetObjectPropertyName,
            get_class($targetObject),
            SourceNamingConventions::class,
            print_r($sourceArray, true)
        );

        throw new SourcePropertyValidationException($errorMessage);
    }

    /**
     * Assign the validated source values to the target object properties.
     *
     * @param array $validatedProperties Collection of the validated source properties
     * @param object $targetObject The target object to map the source values onto
     * @param array $cachedPropertyInformationCollection The cached information
     * collection about the target objects properties
     *
     * @return void
     */
    private function assignValidatedSourceValuesToTargetObjectProperties(
        array $validatedProperties,
        object $targetObject,
        array $cachedPropertyInformationCollection
    ): void {
        if (!$this->isUsingTargetObjectSetters) {
            foreach ($validatedProperties as $propertyName => $newPropertyValue) {
                $cachedPropertyInformationCollection
                [$propertyName][CachedProperty::REFLECTION_INSTANCE]
                    ->setValue($targetObject, $newPropertyValue);
            }

            return;
        }

        // The service uses the target objects setter
        foreach ($validatedProperties as $propertyName => $newPropertyValue) {
            if (
                isset(
                    $cachedPropertyInformationCollection
                    [$propertyName][CachedProperty::SETTER_FUNCTION_NAME]
                )
            ) {
                $targetObject->{
                    $cachedPropertyInformationCollection
                    [$propertyName][CachedProperty::SETTER_FUNCTION_NAME]
                }($newPropertyValue);
            } else {
                $cachedPropertyInformationCollection
                [$propertyName][CachedProperty::REFLECTION_INSTANCE]
                    ->setValue($targetObject, $newPropertyValue);
            }
        }
    }
}
