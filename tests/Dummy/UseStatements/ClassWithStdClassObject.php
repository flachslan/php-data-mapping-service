<?php

/**  @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Dummy\UseStatements;

use stdClass;

/**
 * Class ClassWithStdClassObject
 *
 * @package Oktavlachs\DataMappingService\Tests\Dummy\UseStatements
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class ClassWithStdClassObject
{
    /**
     * @var array<int, stdClass>
     */
    private array $someArray = [];

    private ?stdClass $someStdClass = null;
}
