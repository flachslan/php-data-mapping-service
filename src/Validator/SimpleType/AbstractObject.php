<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\SimpleType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\PropertyTypeValidatorInterface;
use ReflectionClass;
use ReflectionException;

/**
 * A validator for abstract object type values.
 *
 * @package Oktavlachs\DataMappingService\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class AbstractObject implements PropertyTypeValidatorInterface
{
    private string $abstractType;

    /**
     * AbstractObject constructor.
     *
     * @param string $abstractType The full class name of the abstract object type
     */
    public function __construct(string $abstractType)
    {
        try {
            $abstractArrayElementClass = new ReflectionClass($abstractType);
        } catch (ReflectionException $e) {
            $errorMessage = sprintf(
                "specificType '%s' is not supported.",
                $abstractType
            );

            throw new InvalidArgumentException($errorMessage, $e->getCode(), $e);
        }

        if (!$abstractArrayElementClass->isAbstract()) {
            $errorMessage = sprintf(
                "Only abstract classes are supported. Provided type is: '%s'.",
                $abstractType
            );

            throw new InvalidArgumentException($errorMessage);
        }


        if (!str_starts_with($abstractType, "\\")) {
            $abstractType = "\\$abstractType";
        }

        $this->abstractType = $abstractType;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): mixed
    {
        if ($value instanceof $this->abstractType) {
            return $value;
        }

        if (is_array($value)) {
            $newValue = eval(
                sprintf('return new class () extends %s {};', $this->abstractType)
            );
            $dataMappingService->mapArrayOntoObject($value, $newValue);
            return $newValue;
        }

        $message = sprintf(
            "The provided value '%s' is not of type '%s' "
            . 'or an array representation of that type.',
            print_r($value, true),
            $this->abstractType
        );

        throw new InvalidArgumentException($message);
    }
}
