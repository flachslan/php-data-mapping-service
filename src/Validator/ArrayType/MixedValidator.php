<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\AbstractPropertyTypeValidator;

/**
 * A validator for array with elements of a mixed type.
 *
 * @package Oktavlachs\DataMappingService\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class MixedValidator extends AbstractPropertyTypeValidator
{
    private string $arrayIndexType;

    /**
     * Scalar constructor.
     *
     * @param string $arrayIndexType The type of the arrays index
     */
    public function __construct(string $arrayIndexType)
    {
        $this->validateArrayIndexType($arrayIndexType);
        $this->arrayIndexType = $arrayIndexType;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): array
    {
        if (!is_array($value)) {
            $errorMessage = sprintf(
                "The provided value '%s' is not an array.",
                print_r($value, true)
            );

            throw new InvalidArgumentException($errorMessage);
        }

        foreach ($value as $index => $element) {
            if ($this->getType($index) !== $this->arrayIndexType) {
                $errorMessage = sprintf(
                    "Invalid index type '%s'. Expected '%s'",
                    $this->getType($index),
                    $this->arrayIndexType
                );

                throw new InvalidArgumentException($errorMessage);
            }
        }

        return $value;
    }
}
