<?php

/**  @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Dummy\UseStatements;

use Oktavlachs\DataMappingService\Tests\Dummy\Class1;

/**
 * Class ClassWithSimpleUseStatement
 *
 * @package Oktavlachs\DataMappingService\Tests\Dummy\UseStatements
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class ClassWithSimpleUseStatement
{
    /**
     * @var array<int, Class1>
     */
    private array $someArray;
}
