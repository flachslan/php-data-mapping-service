<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Dummy;

/**
 * Class DummyAbstractClass
 *
 * @package Oktavlachs\DataMappingService\Tests\Dummy
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
abstract class DummyAbstractClass
{
    public string $bla;
}
