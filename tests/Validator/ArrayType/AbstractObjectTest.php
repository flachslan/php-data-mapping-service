<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Tests\Dummy\{Class2,
    UseStatements\AbstractClass,
    UseStatements\AbstractClassImplementation};
use Oktavlachs\DataMappingService\Validator\ArrayType\AbstractObject;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Class AbstractObjectTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class AbstractObjectTest extends TestCase
{
    /**
     * @var DataMappingService|MockObject
     */
    private $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            AbstractObject::class,
            new AbstractObject('int', AbstractClass::class)
        );
    }

    public function testConstructThrowsExceptionOnInvalidIndexType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new AbstractObject('sdtClass', 'string');
    }

    public function testConstructThrowsExceptionOnNonObjectElementType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new AbstractObject('int', 'object');
    }

    public function testConstructOnlyAllowAbstractClasses(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new AbstractObject('int', Class2::class);
    }

    public function testValidateWithValidObjects(): void
    {
        $validator = new AbstractObject('int', AbstractClass::class);

        $expected = [
            new AbstractClassImplementation(),
            new class () extends AbstractClass {
            }
        ];

        $this->assertSame(
            $expected,
            $validator->validate($this->dataMappingService, $expected)
        );
    }

    public function testValidateWithValidObjectsInArrayRepresentation(): void
    {
        $validator = new AbstractObject('int', AbstractClass::class);

        $value = [['someString' => 'anything']];

        foreach ($validator->validate($this->dataMappingService, $value) as $result) {
            $this->assertInstanceOf(AbstractClass::class, $result);
        }
    }

    public function testValidateThrowsExceptionOnWrongValueElementType(): void
    {
        $validator = new AbstractObject('int', AbstractClass::class);

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, [3.3]);
    }

    public function testValidateThrowsExceptionOnWrongValueType(): void
    {
        $validator = new AbstractObject('int', AbstractClass::class);

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, 3);
    }

    public function testValidateThrowsExceptionOnWrongValueIndexType(): void
    {
        $validator = new AbstractObject('int', AbstractClass::class);

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, ['someStringIndex' => 3]);
    }
}
