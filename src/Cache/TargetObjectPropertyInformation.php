<?php

/**
 * @copyright 2020 Lukas Stermann
 *
 * @noinspection UnknownInspectionInspection The [EA] plugin doesn't know
 * the noinspection annotation.
 */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Cache;

use ReflectionClass;
use ReflectionProperty;
use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Collection\CachedProperty;
use Oktavlachs\DataMappingService\Reflector\ExtendedReflectionClass;
use Oktavlachs\DataMappingService\Validator\Factory as ValidatorFactory;
use Oktavlachs\DataMappingService\Exception\TargetPropertyInspectionException;

/**
 * A Cache for target object property information.
 *
 * @package Oktavlachs\DataMappingService\Cache
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class TargetObjectPropertyInformation
{
    /**
     * @var array<string, array<string, array<int, mixed>>> Equals
     * array<className, array<propertyName, <integerKey, propertyInformation>>>
     *
     * For information on the indices:
     * @see CachedProperty
     */
    private array $targetObjectPropertyInformationCache = [];

    private ValidatorFactory $validatorFactory;

    private static TargetObjectPropertyInformation $singletonInstance;

    private function __construct()
    {
        $this->validatorFactory = ValidatorFactory::getInstance();
    }

    private function __clone()
    {
    }

    /**
     * TargetObjectPropertyInformation constructor.
     *
     * @return TargetObjectPropertyInformation The only instance of this class.
     */
    public static function getInstance(): TargetObjectPropertyInformation
    {
        if (isset(self::$singletonInstance)) {
            return self::$singletonInstance;
        }

        self::$singletonInstance = new self();
        return self::$singletonInstance;
    }

    /**
     * Get the cached target object property information.
     *
     * @param object $targetObject The target object
     *
     * @return array<string, array<int, mixed>> The cached property
     * information collection
     *
     * @throws TargetPropertyInspectionException In case the
     * inspection of a target property fails
     *
     * @noinspection PhpDocMissingThrowsInspection The ReflectionException
     * is never thrown.
     */
    public function get(object $targetObject): array
    {
        $className = get_class($targetObject);

        // Search for existing property information in cache
        if (isset($this->targetObjectPropertyInformationCache[$className])) {
            return $this->targetObjectPropertyInformationCache[$className];
        }

        $cachedTargetObjectPropertyInformationCollection = [];

        /** @noinspection PhpUnhandledExceptionInspection */
        $reflectionClass = new ReflectionClass(new $className());

        $classUnderPropertyInspection = $reflectionClass;

        do {
            /** @noinspection PhpUnhandledExceptionInspection */
            $classUnderPropertyInspection = new ExtendedReflectionClass(
                "\\" . $classUnderPropertyInspection->getName()
            );

            foreach (
                $classUnderPropertyInspection->getProperties() as $reflectionProperty
            ) {
                $this->inspectTargetObjectProperty(
                    $targetObject,
                    $classUnderPropertyInspection,
                    $reflectionProperty,
                    $cachedTargetObjectPropertyInformationCollection
                );
            }
        } while (
            ($classUnderPropertyInspection =
                $classUnderPropertyInspection->getParentClass())
            !== false
        );

        /*
         * Store the collected information in the cache if
         * the class under property inspection is not anonymous.
         * Anonymous classes would create a new cache entry for
         * each instance, which cause memory leaks.
         */
        if (!$reflectionClass->isAnonymous()) {
            $this->targetObjectPropertyInformationCache[$className] =
                $cachedTargetObjectPropertyInformationCollection;
        }

        return $cachedTargetObjectPropertyInformationCollection;
    }

    /**
     * Inspect a target object property.
     *
     * Save mandatory information in the cached property information collection.
     *
     * @param object $targetObject The target object to map onto.
     * @param ExtendedReflectionClass $extendedReflectionObject Provides information
     * about use statements for array validators
     * @param ReflectionProperty $reflectionPropertyInstance The target object property
     * in a reflection class representation
     * @param array<int, mixed> $cachedTargetObjectPropertyInformationCollection The
     * cached target object property information collection
     *
     * @return void
     *
     * @throws TargetPropertyInspectionException In case the target
     * property inspection fails
     */
    private function inspectTargetObjectProperty(
        object $targetObject,
        ExtendedReflectionClass $extendedReflectionObject,
        ReflectionProperty $reflectionPropertyInstance,
        array &$cachedTargetObjectPropertyInformationCollection
    ): void {
        $propertyName = $reflectionPropertyInstance->getName();

        /*
         * Properties which are overwritten in child classes will
         * appear twice, so the child class one MUST be chosen.
         */
        if (isset($cachedTargetObjectPropertyInformationCollection[$propertyName])) {
            return;
        }

        try {
            $validator = $this->validatorFactory->create(
                $extendedReflectionObject,
                $reflectionPropertyInstance
            );
        } catch (InvalidArgumentException $e) {
            $errorMessage = sprintf(
                "Error while inspecting property '%s' in class '%s'",
                $propertyName,
                $reflectionPropertyInstance->getDeclaringClass()->getName()
            );

            throw new TargetPropertyInspectionException($errorMessage, $e->getCode(), $e);
        }

        // Property has type declaration >= PHP 7.4 style
        if ($reflectionPropertyInstance->hasType()) {
            /**
             * @noinspection UnknownInspectionInspection The noinspection
             * annotation is unknown by the [EA] plugin in PhpStorm.
             * @noinspection NullPointerExceptionInspection Type existence
             * is checked beforehand by ReflectionProperty::hasType().
             */
            $allowsNull = $reflectionPropertyInstance->getType()->allowsNull();
            // Property has another allowed annotated type (e.g. mixed)
        } else {
            $allowsNull = true;
        }

        /*
         * This does ONLY affect the reflection
         * property object, not the object under reflection.
         */
        $reflectionPropertyInstance->setAccessible(true);

        $camelCasePropertyName = $this->toCamelCase($propertyName);
        $lowerSnakeCasePropertyName = $this->toLowerSnakeCase($propertyName);
        $pascalCasePropertyName = ucfirst($camelCasePropertyName);

        $cachedTargetPropertyInformation = [
            CachedProperty::VALIDATOR =>
                $validator,
            CachedProperty::REFLECTION_INSTANCE =>
                $reflectionPropertyInstance,
            CachedProperty::ALLOWS_NULL =>
                $allowsNull,
            CachedProperty::IS_INITIALIZED =>
                $reflectionPropertyInstance->isInitialized($targetObject),
            CachedProperty::NAME_LOWER_SNAKE_CASE =>
                $lowerSnakeCasePropertyName,
            CachedProperty::NAME_CAMEL_CASE =>
                $camelCasePropertyName,
            CachedProperty::NAME_PASCAL_CASE =>
                $pascalCasePropertyName,
            CachedProperty::NAME_UPPER_SNAKE_CASE =>
                strtoupper($lowerSnakeCasePropertyName),
            CachedProperty::NAME_KEBAP_CASE =>
                str_replace('_', '-', $lowerSnakeCasePropertyName)
        ];

        /**
         * We set setter method names in the property information anyway.
         * Otherwise, we would have to make sure through checks, that switching
         * @see DataMappingService::setIsUsingTargetObjectSetters()
         * twice doesn't have side effects on the service.
         */
        $propertySetterMethodName = 'set' . $pascalCasePropertyName;

        if ($extendedReflectionObject->hasMethod($propertySetterMethodName)) {
            $cachedTargetPropertyInformation[CachedProperty::SETTER_FUNCTION_NAME] =
                $propertySetterMethodName;
        }

        $cachedTargetObjectPropertyInformationCollection[$propertyName] =
            $cachedTargetPropertyInformation;
    }

    /**
     * Convert a property name into a string following
     * the lower_snake_case naming convention.
     *
     * @param string $string The string to convert
     *
     * @return string The lower snake case string
     */
    private function toLowerSnakeCase(string $string): string
    {
        // The first character must be a lower case letter
        $result = lcfirst($string[0]);

        for ($index = 1, $length = strlen($string); $index < $length; $index++) {
            if (ctype_upper($string[$index])) {
                $result .= '_' . lcfirst($string[$index]);
                continue;
            }

            $result .= $string[$index];
        }

        return $result;
    }

    /**
     * Convert a string into a string following the camelCase naming convention.
     *
     * @param string $string The string to convert
     *
     * @return string The lower camel case string
     */
    private function toCamelCase(string $string): string
    {
        $result = ucwords($string, " \t\r\n\f\v_-");

        // The first character must be a lower case letter
        return str_replace([' ', '-', '_'], '', lcfirst($result));
    }
}
