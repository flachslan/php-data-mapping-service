<?php

/** @copyright 2020 Lukas Stermann*/

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Dummy\UseStatements;

/**
 * Class SomeParentUseStatementClass
 *
 * @package Oktavlachs\DataMappingService\Tests\Dummy\UseStatements
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class SomeParentUseStatementClass
{
}
