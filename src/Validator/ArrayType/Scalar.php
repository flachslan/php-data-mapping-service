<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\AbstractPropertyTypeValidator;

/**
 * A validator for array with elements of a scalar type.
 *
 * @package Oktavlachs\DataMappingService\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class Scalar extends AbstractPropertyTypeValidator
{
    private string $arrayElementType;

    private string $arrayIndexType;

    /**
     * Scalar constructor.
     *
     * @param string $arrayIndexType The type of the arrays index
     * @param string $arrayElementType The type of the arrays elements
     */
    public function __construct(
        string $arrayIndexType,
        string $arrayElementType
    ) {
        $this->validateArrayIndexType($arrayIndexType);
        $this->arrayIndexType = $arrayIndexType;

        if (!$this->representsScalarType($arrayElementType)) {
            $message = sprintf(
                "The array type '%s' is not a scalar type",
                $arrayElementType
            );

            throw new InvalidArgumentException($message);
        }

        $this->arrayElementType = $arrayElementType;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): array
    {
        if (!is_array($value)) {
            $errorMessage = sprintf(
                "The provided value '%s' is not an array.",
                print_r($value, true)
            );

            throw new InvalidArgumentException($errorMessage);
        }

        foreach ($value as $index => $element) {
            if ($this->getType($index) !== $this->arrayIndexType) {
                $errorMessage = sprintf(
                    "Invalid index type '%s'. Expected '%s'",
                    $this->getType($index),
                    $this->arrayIndexType
                );

                throw new InvalidArgumentException($errorMessage);
            }

            if ($this->getType($element) !== $this->arrayElementType) {
                $errorMessage = sprintf(
                    "The provided value at index '%s' => '%s' "
                    . "is not of type '%s' in '%s'",
                    $index,
                    print_r($element, true),
                    $this->arrayElementType,
                    print_r($value, true)
                );

                throw new InvalidArgumentException($errorMessage);
            }
        }

        return $value;
    }
}
