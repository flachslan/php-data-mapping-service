<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\SimpleType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Tests\Dummy\Class1;
use Oktavlachs\DataMappingService\Validator\SimpleType\StdClassObject;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * Class SpecificObjectTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class StdClassObjectTest extends TestCase
{
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(StdClassObject::class, new StdClassObject());
    }

    public function testValidate(): void
    {
        $validator = new StdClassObject();

        $expected = new stdClass();
        $expected->someProperty = 'someValue';
        $expected->otherProperty = 'otherValue';

        $this->assertEquals(
            $expected,
            $validator->validate(
                $this->dataMappingService,
                ['someProperty' => 'someValue', 'otherProperty' => 'otherValue']
            )
        );

        $this->assertEquals(
            $expected,
            $validator->validate($this->dataMappingService, $expected)
        );
    }

    public function testValidateWithWrongValue(): void
    {
        $validator = new StdClassObject();

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, new Class1());
    }
}
