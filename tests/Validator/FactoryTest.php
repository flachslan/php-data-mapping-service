<?php

/**
 * @copyright 2020 Lukas Stermann
 *
 * @noinspection UnknownInspectionInspection
 */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Oktavlachs\DataMappingService\Validator\Factory;
use Oktavlachs\DataMappingService\Tests\Dummy\Class2;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\ArrayType\Nested;
use Oktavlachs\DataMappingService\Tests\Dummy\{DummyAbstractClass,
    ClassWithMixedTypes,
    UseStatements\ClassWithSameLevelNameSpaceAbstractArrayElement,
    UseStatements\ClassWithSameLevelNameSpaceArrayElement};
use Oktavlachs\DataMappingService\Reflector\ExtendedReflectionClass;
use Oktavlachs\DataMappingService\Tests\Dummy\UseStatements\AbstractClass;
use Oktavlachs\DataMappingService\Validator\PropertyTypeValidatorInterface;
use Oktavlachs\DataMappingService\Tests\Dummy\UseStatements\ClassWithStdClassObject;
use Oktavlachs\DataMappingService\Tests\Dummy\UseStatements\ClassWithSimpleUseStatement;
use Oktavlachs\DataMappingService\Validator\SimpleType\{AbstractObject,
    MixedValidator,
    Scalar,
    SpecificObject,
    StdClassObject};
use ReflectionException;
use stdClass;

/**
 * Class FactoryTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class FactoryTest extends TestCase
{
    private Factory $factory;

    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();

        $this->factory = Factory::getInstance();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(Factory::class, $this->factory);
    }

    public function testCreateWithScalarType(): void
    {
        $target = new class () {
            public string $scalar;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->assertInstanceOf(
                Scalar::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('scalar')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /** @noinspection PhpMissingFieldTypeInspection Part of the test*/
    public function testCreateWithMixedType(): void
    {
        $target = new class () {
            /**
             * @var mixed
             */
            public mixed $mixed;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);
            $this->assertInstanceOf(
                MixedValidator::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('mixed')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testCreateWithObject(): void
    {
        $target = new class () {
            public Class2 $object;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->assertInstanceOf(
                SpecificObject::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('object')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testCreateWithAbstractObject(): void
    {
        $target = new class () {
            public AbstractClass $object;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->assertInstanceOf(
                AbstractObject::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('object')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testCreateWithStdClassObject(): void
    {
        $target = new class () {
            public stdClass $object;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->assertInstanceOf(
                StdClassObject::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('object')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /**
     * @noinspection PhpUnused Purpose of this test
     * @noinspection PhpMissingFieldTypeInspection Purpose of this test
     */
    public function testCreateFailsWithMissingPropertyType(): void
    {
        $target = new class () {
            public $noType;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);
            $this->expectException(InvalidArgumentException::class);
            $this->factory->create(
                $reflectionTarget,
                $reflectionTarget->getProperty('noType')
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /** @noinspection PhpUnused Not part of this test */
    public function testCreateFailsWithInvalidType(): void
    {
        $target = new class () {
            public object $invalidType;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);
            $this->expectException(InvalidArgumentException::class);
            $this->factory->create(
                $reflectionTarget,
                $reflectionTarget->getProperty('invalidType')
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /** @noinspection PhpUnused Accessing properties is not part of this test */
    public function testCreateFailsWithEmptyDocComment(): void
    {
        $target = new class () {
            public array $noDocComment;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);
            $this->expectException(InvalidArgumentException::class);
            $this->factory->create(
                $reflectionTarget,
                $reflectionTarget->getProperty('noDocComment')
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /** @noinspection PhpUnused Not part of this test */
    public function testCreateFailsWithInvalidPropertyType(): void
    {
        $target = new class () {
            public object $invalidType;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);
            $this->expectException(InvalidArgumentException::class);

            $this->factory->create(
                $reflectionTarget,
                $reflectionTarget->getProperty('invalidType')
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /** @noinspection PhpUnused Accessing properties is not part of this test */
    public function testCreateWithScalarArray(): void
    {
        $target = new class () {
            /**
             * @var array<int, string>
             */
            public array $scalarArray;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);
            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\Scalar::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('scalarArray')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /**
     * @noinspection PhpUnused Accessing properties is not part of this test
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection The full name space
     * is only required because the test uses an anonymous class.
     */
    public function testCreateWithSpecificObjectArray(): void
    {
        $target = new class () {
            /**
             * @var array<int, \Oktavlachs\DataMappingService\Tests\Dummy\Class2>
             */
            public array $specificObjectType;

            /**
             * @var array<int, Class2>
             */
            public array $specificObjectTypeWithNameSpace;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);
            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\SpecificObject::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('specificObjectType')
                )
            );
            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\SpecificObject::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('specificObjectTypeWithNameSpace')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /**
     * @noinspection PhpUnused Accessing properties is not part of this test
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection The full name space
     * is only required because the test uses an anonymous class.
     */
    public function testCreateWithAbstractObjectArray(): void
    {
        $target = new class () {
            /**
             * @var array<int, \Oktavlachs\DataMappingService\Tests\Dummy\DummyAbstractClass>
             */
            public array $abstractObjectType;

            /**
             * @var array<int, DummyAbstractClass>
             */
            public array $abstractObjectTypeWithNameSpace;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);
            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\AbstractObject::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('abstractObjectType')
                )
            );
            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\AbstractObject::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('abstractObjectTypeWithNameSpace')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testCreateWithStdClassObjectArray(): void
    {
        $target = new ClassWithStdClassObject();

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\StdClassObject::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('someArray')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testCreateWithMixedArray(): void
    {
        $target = new ClassWithMixedTypes();

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\MixedValidator::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('mixedArrayType')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /** @noinspection PhpUnused Accessing properties is not part of this test */
    public function testCreateWithNestedArray(): void
    {
        $target = new class () {
            /**
             * @var array<int, array<string, bool>>
             */
            public array $nestedArray;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->assertInstanceOf(
                Nested::class,
                $this->factory->create(
                    $reflectionTarget,
                    $reflectionTarget->getProperty('nestedArray')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /** @noinspection PhpUnused Accessing properties is not part of this test */
    public function testCreateWithNestedInvalidArray(): void
    {
        $target = new class () {
            /**
             * @var array<int, object>
             */
            public array $nestedArray;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->expectException(InvalidArgumentException::class);
            $this->factory->create(
                $reflectionTarget,
                $reflectionTarget->getProperty('nestedArray')
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    /** @noinspection PhpUnused Accessing properties is not part of this test */
    public function testCreateWithInvalidDocComment(): void
    {
        $target = new class () {
            /**
             * @var array<int>
             */
            public array $nestedArray;
        };

        try {
            $reflectionTarget = new ExtendedReflectionClass($target);

            $this->expectException(InvalidArgumentException::class);
            $this->factory->create(
                $reflectionTarget,
                $reflectionTarget->getProperty('nestedArray')
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testCreateWithClassOnSameNamespaceLevel(): void
    {
        try {
            $extendedReflectionObject = new ExtendedReflectionClass(
                new ClassWithSameLevelNameSpaceArrayElement()
            );

            $AbstractExtendedReflectionObject = new ExtendedReflectionClass(
                new ClassWithSameLevelNameSpaceAbstractArrayElement()
            );

            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\SpecificObject::class,
                $this->factory->create(
                    $extendedReflectionObject,
                    $extendedReflectionObject->getProperty('someArray')
                )
            );

            $this->assertInstanceOf(
                \Oktavlachs\DataMappingService\Validator\ArrayType\AbstractObject::class,
                $this->factory->create(
                    $AbstractExtendedReflectionObject,
                    $AbstractExtendedReflectionObject->getProperty('someArray')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }

    public function testCreateWithArrayElementTypeFromUseStatement(): void
    {
        try {
            $extendedReflectionObject = new ExtendedReflectionClass(
                new ClassWithSimpleUseStatement()
            );

            /**
             * @noinspection UnnecessaryAssertionInspection We only want to make sure the
             * function returns with an instance and does not throw an exception.
             */
            $this->assertInstanceOf(
                PropertyTypeValidatorInterface::class,
                $this->factory->create(
                    $extendedReflectionObject,
                    $extendedReflectionObject->getProperty('someArray')
                )
            );
        } catch (ReflectionException $e) {
            // Never thrown.
        }
    }
}
