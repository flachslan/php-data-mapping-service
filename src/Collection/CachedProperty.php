<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Collection;

/**
 * Collection of cached class property names.
 *
 * @package Oktavlachs\DataMappingService\Collection
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class CachedProperty
{
    public const VALIDATOR = 0;

    public const REFLECTION_INSTANCE = 1;

    public const ALLOWS_NULL = 2;

    public const IS_INITIALIZED = 3;

    public const NAME_LOWER_SNAKE_CASE = 4;

    public const NAME_CAMEL_CASE = 5;

    public const NAME_PASCAL_CASE = 6;

    public const NAME_UPPER_SNAKE_CASE = 7;

    public const NAME_KEBAP_CASE = 8;

    public const SETTER_FUNCTION_NAME = 9;
}
