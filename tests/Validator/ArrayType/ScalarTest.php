<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use PHPUnit\Framework\TestCase;
use Oktavlachs\DataMappingService\Validator\ArrayType\Scalar;

/**
 * Class ScalarTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class ScalarTest extends TestCase
{
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            Scalar::class,
            new Scalar('int', 'string')
        );
    }

    public function testConstructThrowsExceptionOnInvalidIndexType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Scalar('sdtClass', 'string');
    }

    public function testConstructThrowsExceptionOnNonScalarElementType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Scalar('int', 'object');
    }

    public function testValidate(): void
    {
        $validator = new Scalar('int', 'float');
        $this->assertSame([3.3], $validator->validate($this->dataMappingService, [3.3]));
    }

    public function testValidateThrowsExceptionOnWrongValueElementType(): void
    {
        $validator = new Scalar('int', 'int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, [3.3]);
    }

    public function testValidateThrowsExceptionOnWrongValueType(): void
    {
        $validator = new Scalar('int', 'int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, 3);
    }

    public function testValidateThrowsExceptionOnWrongValueIndexType(): void
    {
        $validator = new Scalar('int', 'int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, ['someStringIndex' => 3]);
    }
}
