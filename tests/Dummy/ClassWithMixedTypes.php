<?php

/**
 * @copyright 2020 Lukas Stermann
 *
 * @noinspection PhpUnusedPrivateFieldInspection
 */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Dummy;

/**
 * Class ClassWithMixedTypes
 *
 * @package Oktavlachs\DataMappingService\Tests\Dummy
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class ClassWithMixedTypes
{
    /**
     * @var mixed
     */
    private mixed $mixedType;

    /**
     * @var array<int, mixed>
     */
    private array $mixedArrayType = [];
}
