<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\SimpleType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\PropertyTypeValidatorInterface;
use ReflectionClass;
use ReflectionException;

/**
 * A validator for specific object type values.
 *
 * @package Oktavlachs\DataMappingService\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class SpecificObject implements PropertyTypeValidatorInterface
{
    private string $specificType;

    /**
     * SpecificObject constructor.
     *
     * @param string $specificType The full class name of the specific object type
     */
    public function __construct(string $specificType)
    {
        try {
            $abstractArrayElementClass = new ReflectionClass($specificType);
        } catch (ReflectionException $e) {
            $errorMessage = sprintf(
                "specificType '%s' is not supported.",
                $specificType
            );

            throw new InvalidArgumentException($errorMessage, $e->getCode(), $e);
        }

        if ($abstractArrayElementClass->isAbstract()) {
            $errorMessage = sprintf(
                "Abstract classes are not supported: '%s'.",
                $specificType
            );

            throw new InvalidArgumentException($errorMessage);
        }


        if (!str_starts_with($specificType, "\\")) {
            $specificType = "\\$specificType";
        }

        $this->specificType = $specificType;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): mixed
    {
        if ($value instanceof $this->specificType) {
            return $value;
        }

        if (is_array($value)) {
            $newValue = new $this->specificType();
            $dataMappingService->mapArrayOntoObject($value, $newValue);
            return $newValue;
        }

        $message = sprintf(
            "The provided value '%s' is not of type '%s' "
            . 'or an array representation of that type.',
            print_r($value, true),
            $this->specificType
        );

        throw new InvalidArgumentException($message);
    }
}
