<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Reflector;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;

/**
 * Provide additional functionality to the PHP ReflectionObject class.
 *
 * @package Oktavlachs\DataMappingService\Reflector
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class ExtendedReflectionClass extends ReflectionClass
{
    /**
     * @var array<string, string> Where this means
     * array<classNameOrAlias, fullClassNamespace>
     */
    private array $useStatements = [];

    /**
     * Flag, indicating that this object has been
     * parsed for use statements already.
     *
     * @var bool
     */
    private bool $isParsedForUseStatements = false;

    /**
     * ExtendedReflectionObject constructor.
     *
     * @param object|string $objectOrString The object under reflection
     *
     * @throws ReflectionException In case ReflectionClass::construct()
     * throws an exception
     */
    public function __construct(object|string $objectOrString)
    {
        parent::__construct($objectOrString);

        if (!$this->isUserDefined()) {
            if (is_object($objectOrString)) {
                $objectOrString = get_class($objectOrString);
            }

            $errorMessage = sprintf(
                "The object '%s' is not a user defined class.",
                $objectOrString
            );

            throw new InvalidArgumentException($errorMessage);
        }
    }

    /**
     * Parse the use statements from a file defining the objects class.
     *
     * @return void
     */
    private function parseUseStatements(): void
    {
        $classFileHeader = $this->parseClassFileHeader();

        $numberOfUseStatements = preg_match_all(
            "/use (?'statementNamespace'[\w\\\][\n\r\f\v\t{, }\w\\\]*);/",
            $classFileHeader,
            $matches
        );

        // No use statements have been declared in the class
        if (($numberOfUseStatements === false) || ($numberOfUseStatements === 0)) {
            return;
        }

        foreach ($matches['statementNamespace'] as $statementNamespace) {
            $this->inspectUseStatementNamespace($statementNamespace);
        }
    }

    /**
     * Parse the header part of the file which defines the objects class.
     *
     * @return string The parsed class file header
     */
    private function parseClassFileHeader(): string
    {
        /*
         * self::getFileName() can never resolve as false because we
         * only allow user defined classes in this class.
         */
        $classFile = fopen((string) $this->getFileName(), 'rb');

        $classFileHeader = '';

        for (
            $lineIndex = 1,
            $startLine = $this->getStartLine(); $lineIndex < $startLine; $lineIndex++
        ) {
            $classFileHeader .= fgets($classFile);
        }

        fclose($classFile);

        return $classFileHeader;
    }

    /**
     * Inspect a use statement namespace.
     *
     * Parse for normal and grouped use statement namespaces.
     *
     * @param string $namespace The namespace to inspect
     *
     * @return void
     */
    private function inspectUseStatementNamespace(string $namespace): void
    {
        $curlyBracketsPosition = strpos($namespace, '{');

        // No grouped use statements have been used
        if ($curlyBracketsPosition === false) {
            if (!str_starts_with($namespace, "\\")) {
                $namespace = "\\$namespace";
            }

            // Use statement like 'use Foo\Bar\MyClass as Schwifty'
            if (($asPosition = strrpos($namespace, ' as ')) !== false) {
                $keyClassName = substr($namespace, $asPosition + 4);
                $namespace = substr($namespace, 0, $asPosition);
            // Use statement like 'use Foo\Bar\MyClass'
            } elseif (($lastBackSlashPosition = (int) strrpos($namespace, "\\")) > 0) {
                $keyClassName = substr($namespace, $lastBackSlashPosition + 1);
            // Use statement like 'use stdClass'
            } else {
                // Without the leading \ character
                $keyClassName = substr($namespace, 1);
            }

            $this->useStatements[$this->deleteLineBreaksAndSpaces($keyClassName)] =
                $this->deleteLineBreaksAndSpaces($namespace);

            return;
        }

        // Separate the general name space from the grouped use statements
        $generalNameSpace =
            $this->deleteLineBreaksAndSpaces(
                substr($namespace, 0, $curlyBracketsPosition)
            );

        if (!str_starts_with($generalNameSpace, "\\")) {
            $generalNameSpace = "\\$generalNameSpace";
        }

        $groupedUseStatements = substr($namespace, $curlyBracketsPosition);

        /*
         * Excerpt class names from use statements.
         * There is no way this will not match, because of the preg_match()
         * beforehand, fail check is not necessary.
         */
        preg_match_all(
            "/(?'className'([\w\\\]+ as [\w\\\]+)|[\w\\\]+),?/",
            $groupedUseStatements,
            $groupedUseStatementsMatches
        );

        foreach ($groupedUseStatementsMatches['className'] as $className) {
            // Grouped use statements like 'use Foo\{Bar as Schwifty,...}'
            if (($asPosition = strpos($className, ' as ')) !== false) {
                $keyClassName = substr($className, $asPosition + 4);
                $className = substr($className, 0, $asPosition);
            // Grouped use statements like 'use Foo\{Bar\Schwifty,...}'
            } elseif (($lastBackSlashPosition = (int) strrpos($className, "\\")) > 0) {
                $keyClassName = substr($className, $lastBackSlashPosition + 1);
            // Grouped use statements like 'use Foo\{Bar, ...}'
            } else {
                $keyClassName = $className;
            }

            $this->useStatements[$this->deleteLineBreaksAndSpaces($keyClassName)] =
                $generalNameSpace . $this->deleteLineBreaksAndSpaces($className);
        }
    }

    /**
     * Delete line breaks and space characters from a string.
     *
     * @param string $string The string to delete the characters from
     *
     * @return string The string without the characters
     */
    private function deleteLineBreaksAndSpaces(string $string): string
    {
        return str_replace(["\n", ' ', "\t", "\v", "\f", "\r"], '', $string);
    }

    /**
     * Return a use statement for a certain class name defined in the objects class file.
     *
     * @var string $className The name of the class to get the use statement for
     *
     * @return string|null The statement for the provided
     * class name or null if it does not exist.
     */
    public function getUseStatement(string $className): ?string
    {
        if (!$this->isParsedForUseStatements) {
            $this->parseUseStatements();
            $this->isParsedForUseStatements = true;
        }

        return $this->useStatements[$className] ?? null;
    }
}
