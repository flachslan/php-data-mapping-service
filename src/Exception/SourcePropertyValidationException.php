<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Exception;

/**
 * Wraps exceptions which happen during the validation of a source property.
 *
 * @package Oktavlachs\DataMappingService\Exception
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class SourcePropertyValidationException extends DataMappingServiceException
{
}
