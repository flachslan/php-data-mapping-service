<?php

/**
 * @copyright 2020 Lukas Stermann
 *
 * @noinspection UnknownInspectionInspection The [EA] plugin doesn't know
 * the noinspection annotation.
 */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator;

use stdClass;
use ReflectionClass;
use ReflectionProperty;
use InvalidArgumentException;
use Oktavlachs\DataMappingService\Validator\ArrayType\Nested;
use Oktavlachs\DataMappingService\Reflector\ExtendedReflectionClass;
use Oktavlachs\DataMappingService\Validator\SimpleType\{AbstractObject,
    MixedValidator,
    Scalar,
    SpecificObject,
    StdClassObject};

/**
 * Factory which returns a PropertyTypeValidatorInterface, depending on a given type.
 *
 * @package Oktavlachs\DataMappingService\Validator
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class Factory
{
    private static Factory $singletonInstance;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * Get the only instance of this class.
     *
     * @return Factory The only instance of this class
     */
    public static function getInstance(): Factory
    {
        if (isset(self::$singletonInstance)) {
            return self::$singletonInstance;
        }

        self::$singletonInstance = new self();
        return self::$singletonInstance;
    }

    /**
     * Create a PropertyTypeValidatorInterface instance by a given property.
     *
     * @param ExtendedReflectionClass $extendedReflectionObject ReflectionObject
     * with use statement parse feature
     * @param ReflectionProperty $property The property under inspection
     *
     * @return PropertyTypeValidatorInterface The adequate
     * property type validator instance
     *
     * @noinspection PhpDocMissingThrowsInspection The ReflectionException
     * is never thrown.
     */
    public function create(
        ExtendedReflectionClass $extendedReflectionObject,
        ReflectionProperty $property
    ): PropertyTypeValidatorInterface {
        // Only accept property type declaration >= PHP 8 style
        if (!$property->hasType()) {
            $errorMessage = sprintf(
                "Property '%s' in class '%s' does not "
                . 'explicitly declare a property type (as in >= PHP 8)',
                $property->getName(),
                $property->getDeclaringClass()->getName()
            );

            throw new InvalidArgumentException($errorMessage);
        }

        /**
         * @noinspection NullPointerExceptionInspection Will not be thrown because
         * we check for that above.
         */
        $typeName = $property->getType()->getName();

        // Set validator for stdClass objects
        if ($typeName === stdClass::class) {
            return new StdClassObject();
        }

        // Set validator for arrays
        if ($typeName === 'array') {
            return $this->createArray(
                $extendedReflectionObject,
                $property
            );
        }

        // Set validator for mixed type
        if ($typeName === 'mixed') {
            return new MixedValidator();
        }

        // Set validator for scalar types
        if ($this->representsScalarType($typeName)) {
            return new Scalar($typeName);
        }

        // Set validator for specific user defined objects
        if (class_exists($typeName)) {
            /**
             * @noinspection PhpUnhandledExceptionInspection A
             * ReflectionException is never thrown.
             */
            $typeReflection = new ReflectionClass($typeName);

            if ($typeReflection->isAbstract()) {
                return new AbstractObject($typeName);
            }

            return new SpecificObject($typeName);
        }

        $errorMessage = sprintf(
            "Property type '%s' is not supported. "
            . "Affected property '%s' in class '%s'",
            $typeName,
            $property->getName(),
            $property->getDeclaringClass()->getName()
        );

        throw new InvalidArgumentException($errorMessage);
    }

    /**
     * Create a AbstractArrayTypeValidator instance by a given property.
     *
     * @param ExtendedReflectionClass $extendedReflectionObject Provides
     * information about use statements.
     * @param ReflectionProperty $property The property under inspection
     *
     * @return PropertyTypeValidatorInterface The adequate property type
     * validator instance
     */
    private function createArray(
        ExtendedReflectionClass $extendedReflectionObject,
        ReflectionProperty $property
    ): PropertyTypeValidatorInterface {
        $propertyName = $property->getName();

        // Casting false results in an empty string
        if (($docComment = (string) $property->getDocComment()) === '') {
            $errorMessage = sprintf(
                "The array property '%s' in class '%s' "
                . "doesn't specify its type in a doc comment.",
                $propertyName,
                $property->getDeclaringClass()->getName()
            );

            throw new InvalidArgumentException($errorMessage);
        }

        [$arrayIndexType, $arrayElementType] =
            $this->parseSpecificArrayType($docComment);

        return $this->createFromIndexAndElementType(
            $arrayIndexType,
            $arrayElementType,
            $extendedReflectionObject
        );
    }

    /**
     * Create a validator from an arrays index and element type.
     *
     * @param string $arrayIndexType The type of the arrays index
     * @param string $arrayElementType The type of the arrays elements
     * @param ExtendedReflectionClass $extendedReflectionObject Provides
     * use statement information of a class.
     *
     * @return PropertyTypeValidatorInterface An array validator instance
     *
     * @noinspection PhpDocMissingThrowsInspection The ReflectionException
     * is never thrown.
     */
    private function createFromIndexAndElementType(
        string $arrayIndexType,
        string $arrayElementType,
        ExtendedReflectionClass $extendedReflectionObject
    ): PropertyTypeValidatorInterface {
        if ($this->representsScalarType($arrayElementType)) {
            return new ArrayType\Scalar($arrayIndexType, $arrayElementType);
        }

        // Nested array types
        if (str_starts_with($arrayElementType, 'array')) {
            [$nextArrayIndexType, $nextArrayElementType] =
                $this->parseSpecificArrayType($arrayElementType);

            return new Nested(
                $arrayIndexType,
                $this->createFromIndexAndElementType(
                    $nextArrayIndexType,
                    $nextArrayElementType,
                    $extendedReflectionObject
                )
            );
        }

        if ($arrayElementType === 'mixed') {
            return new ArrayType\MixedValidator($arrayIndexType);
        }

        if ($arrayElementType === stdClass::class) {
            return new ArrayType\StdClassObject($arrayIndexType);
        }

        if (class_exists($arrayElementType)) {
            /**
             * @noinspection PhpUnhandledExceptionInspection A
             * ReflectionException is never thrown.
             */
            $elementTypeReflection = new ReflectionClass($arrayElementType);

            if ($elementTypeReflection->isAbstract()) {
                return new ArrayType\AbstractObject($arrayIndexType, $arrayElementType);
            }

            return new ArrayType\SpecificObject($arrayIndexType, $arrayElementType);
        }

        /*
         * The element type is an alias of a class name or just
         * a class name defined in a use statement of the class.
         */
        if (
            ($fullClassNameSpaceFromUseStatement =
                $extendedReflectionObject->getUseStatement($arrayElementType))
            !== null
        ) {
            /**
             * @noinspection PhpUnhandledExceptionInspection A
             * ReflectionException is never thrown.
             */
            $fullClassNameSpaceReflection =
                new ReflectionClass($fullClassNameSpaceFromUseStatement);

            if ($fullClassNameSpaceReflection->isAbstract()) {
                return new ArrayType\AbstractObject(
                    $arrayIndexType,
                    $fullClassNameSpaceFromUseStatement
                );
            }

            return new ArrayType\SpecificObject(
                $arrayIndexType,
                $fullClassNameSpaceFromUseStatement
            );
        }

        /*
         * The type cam be a class name without a use statement
         * but exists within the same namespace
         */
        $fullClassNameSpace = sprintf(
            "\\%s\\%s",
            $extendedReflectionObject->getNamespaceName(),
            $arrayElementType
        );

        if (class_exists($fullClassNameSpace)) {
            /**
             * @noinspection PhpUnhandledExceptionInspection A
             * ReflectionException is never thrown.
             */
            $fullClassNameSpaceReflection = new ReflectionClass($fullClassNameSpace);

            if ($fullClassNameSpaceReflection->isAbstract()) {
                return new ArrayType\AbstractObject($arrayIndexType, $fullClassNameSpace);
            }

            return new ArrayType\SpecificObject($arrayIndexType, $fullClassNameSpace);
        }

        $errorMessage = sprintf(
            "The array type '%s' is not supported.",
            $arrayElementType
        );

        throw new InvalidArgumentException($errorMessage);
    }

    /**
     * Parse the specific array type from a given doc comment.
     *
     * @param string $docComment The doc comment to parse from
     *
     * @return array<int, string> The index type and the element type of the array
     */
    private function parseSpecificArrayType(
        string $docComment
    ): array {
        // Parsing for specific array syntax.
        $foundTypes = preg_match(
            "/array<(?'arrayIndexType'int|string), "
            . "(?'arrayElementType'(?R)|\\\?\w[\w\\\]*)>/",
            $docComment,
            $matches
        );

        if ($foundTypes === false || $foundTypes === 0) {
            $errorMessage = sprintf(
                "The given array specification in the doc comment '%s' is invalid. "
                . "It has to follow the format 'array<INDEX_TYPE, ELEMENT_TYPE>' "
                . '(without the single quotes), where INDEX_TYPE is of a valid PHP array '
                . 'key type and ELEMENT_TYPE is of a scalar/user defined object/nested '
                . 'array type.',
                $docComment,
            );

            throw new InvalidArgumentException($errorMessage);
        }

        return [$matches['arrayIndexType'], $matches['arrayElementType']];
    }

    /**
     * Check if a string value represents a scalar PHP type.
     *
     * @param string $string The string to check on
     *
     * @return bool Flag which show if the string represents a scalar type
     */
    private function representsScalarType(string $string): bool
    {
        return match ($string) {
            'string', 'int', 'float', 'bool' => true,
            default => false,
        };
    }
}
