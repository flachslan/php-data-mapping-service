<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Validator\ArrayType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\Validator\{AbstractPropertyTypeValidator,
    PropertyTypeValidatorInterface};
use Oktavlachs\DataMappingService\DataMappingService;

/**
 * Validate values of a specific type.
 *
 * @package Oktavlachs\DataMappingService\Validator\ArrayType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
final class Nested extends AbstractPropertyTypeValidator
{
    private string $arrayIndexType;

    private PropertyTypeValidatorInterface $nextLevelArrayValidator;

    /**
     * Nested constructor.
     *
     * @param string $arrayIndexType The type of the arrays index
     * @param PropertyTypeValidatorInterface $nextLevelArrayValidator The
     * type of the arraysElements
     */
    public function __construct(
        string $arrayIndexType,
        PropertyTypeValidatorInterface $nextLevelArrayValidator
    ) {

        $this->validateArrayIndexType($arrayIndexType);
        $this->arrayIndexType = $arrayIndexType;

        $this->nextLevelArrayValidator = $nextLevelArrayValidator;
    }

    /**
     * @inheritDoc
     */
    public function validate(DataMappingService $dataMappingService, $value): array
    {
        if (!is_array($value)) {
            $message = sprintf(
                "The provided value '%s' is not an array.",
                print_r($value, true)
            );

            throw new InvalidArgumentException($message);
        }

        foreach ($value as $index => &$element) {
            if ($this->getType($index) !== $this->arrayIndexType) {
                $errorMessage = sprintf(
                    "Invalid index type '%s'. Expected '%s'",
                    $index,
                    $this->arrayIndexType
                );

                throw new InvalidArgumentException($errorMessage);
            }

            $element = $this
                ->nextLevelArrayValidator
                ->validate($dataMappingService, $element);
        }

        return $value;
    }
}
