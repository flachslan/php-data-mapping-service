<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\SimpleType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Validator\SimpleType\Scalar;
use PHPUnit\Framework\TestCase;

/**
 * Class ScalarTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class ScalarTest extends TestCase
{
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(Scalar::class, new Scalar('float'));
    }

    public function testConstructDoesntAllowNonScalarTypes(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Scalar('object');
    }

    public function testValidate(): void
    {
        $validator = new Scalar('bool');
        $this->assertTrue($validator->validate($this->dataMappingService, true));
    }

    public function testValidateWithWrongValue(): void
    {
        $validator = new Scalar('int');

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, 'someString');
    }
}
