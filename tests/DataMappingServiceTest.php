<?php

/**
 * @noinspection UnknownInspectionInspection Plugin [EA] does not know the
 * noinspection annotation
 * @noinspection PhpUndefinedClassInspection The correct SimpleXMLElement
 * class is found anyway
 *
 * @copyright 2020 Lukas Stermann
 */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests;

use stdClass;
use ReflectionObject;
use SimpleXMLElement;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Collection\SourceNamingConventions;
use Oktavlachs\DataMappingService\Tests\Dummy\UseStatements\{AbstractClass,
    AbstractClassImplementation};
use Oktavlachs\DataMappingService\Exception\{SourcePropertyValidationException,
    TargetPropertyInspectionException};

/**
 * Class DataMappingServiceTest
 *
 * @package Oktavlachs\DataMappingService\Tests
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class DataMappingServiceTest extends TestCase
{
    /**
     * @var DataMappingService
     */
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testCreate(): void
    {
        $this->assertInstanceOf(DataMappingService::class, $this->dataMappingService);
    }

    /**
     * @noinspection PhpUnused Accessing the property is not part of the test
     * @noinspection PhpMissingFieldTypeInspection This is the tests purpose
     */
    public function testTargetObjectPropertiesMustDefineTypes(): void
    {
        $target = new class () {
            public $uninitialized;
        };

        $source = ['uninitialized' => 'valueFromSource'];

        $this->expectException(TargetPropertyInspectionException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    /** @noinspection PhpUnused Accessing the property is not part of the test */
    public function testTargetObjectPropertyDoesNotAllowNullValueInSource(): void
    {
        $target = new class () {
            public string $uninitialized;
        };

        $source = ['uninitialized' => null];

        $this->expectException(SourcePropertyValidationException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    public function testMapPrivateProperty(): void
    {
        $target = new class () {
            private string $uninitialized;

            public function getUninitialized(): string
            {
                return $this->uninitialized;
            }
        };

        $source = ['uninitialized' => 'valueFromSource'];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertSame('valueFromSource', $target->getUninitialized());
    }

    public function testAcceptCamelCasePropertyNameInSourceObject(): void
    {
        $this->dataMappingService = new DataMappingService(
            SourceNamingConventions::CAMEL_CASE
        );
        $target = new class () {
            public string $SomePascalCasedProperty;
        };

        $source = ['somePascalCasedProperty' => 'valueFromSource'];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertSame('valueFromSource', $target->SomePascalCasedProperty);
    }

    public function testAcceptLowerSnakeCasePropertyNameInSourceObject(): void
    {
        $this->dataMappingService = new DataMappingService(
            SourceNamingConventions::LOWER_SNAKE_CASE
        );

        $target = new class () {
            public string $someProperty;
        };

        $source = ['some_property' => 'valueFromSource'];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertSame('valueFromSource', $target->someProperty);
    }

    public function testAcceptPascalCasePropertyNameInSourceObject(): void
    {
        $this->dataMappingService = new DataMappingService(
            SourceNamingConventions::PASCAL_CASE
        );

        $target = new class () {
            public string $someProperty;
        };

        $source = ['SomeProperty' => 'valueFromSource'];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertSame('valueFromSource', $target->someProperty);
    }

    /** @noinspection PhpUnused Target property access is not in focus of this test */
    public function testDeclineLowerSnakeCasePropertyNameInSourceObject(): void
    {
        $this->dataMappingService = new DataMappingService(
            SourceNamingConventions::TARGET_ONLY
        );

        $target = new class () {
            public string $someProperty;
        };

        $source = ['some_property' => 'valueFromSource'];

        $this->expectException(SourcePropertyValidationException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    public function testDeclaredCaseNotFoundButUseDefaultValue(): void
    {
        $this->dataMappingService = new DataMappingService(
            SourceNamingConventions::TARGET_ONLY
        );

        $target = new class () {
            public string $someProperty = 'init';
        };

        $source = ['some_property' => 'valueFromSource'];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertSame('init', $target->someProperty);
    }

    public function testUninitializedPropertyIsPresentInSource(): void
    {
        $target = new class () {
            public string $uninitialized;
        };

        $source = ['uninitialized' => 'valueFromSource'];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertSame('valueFromSource', $target->uninitialized);
    }

    /**
     * @noinspection PhpUnused The test does not access the anonymous class property
     */
    public function testUninitializedPropertyIsNotPresentInSource(): void
    {
        $target = new class () {
            public string $uninitialized;
        };

        $source = [];

        $this->expectException(SourcePropertyValidationException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    public function testNullableAndUninitializedPropertyIsPresentInSource(): void
    {
        $target = new class () {
            public ?string $nullableAndUninitializedWithNull;

            public ?string $nullableAndUninitializedWithObjectType;
        };

        $source = [
            'nullableAndUninitializedWithNull' => null,
            'nullableAndUninitializedWithObjectType' => 'someValue'
        ];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertNull($target->nullableAndUninitializedWithNull);
        $this->assertSame('someValue', $target->nullableAndUninitializedWithObjectType);
    }

    /**
     * @noinspection PhpUnused The test does not access the anonymous class property
     */
    public function testNullableAndUninitializedPropertyIsNotPresentInSource(): void
    {
        $target = new class () {
            public ?string $nullableAndUninitializedWithNull;
        };

        $source = [];

        $this->expectException(SourcePropertyValidationException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    public function testMapAcceptsSpecifiedArray(): void
    {
        $target = new class () {
            /**
             * @var array<int, string>
             */
            public array $specificArray;
        };

        $testArray = ['test1', 'test2'];
        $source = ['specificArray' => $testArray];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertSame($testArray, $target->specificArray);
    }

    public function testMapAcceptsNullableSpecifiedArray(): void
    {
        $target = new class () {
            /**
             * @var array<int, string>
             */
            public ?array $nullableSpecificArrayWithNull;

            /**
             * @var array<int, string>
             */
            public ?array $nullableSpecificArrayWithArray;
        };

        $testArray = ['test1', 'test2'];
        $source = [
            'nullableSpecificArrayWithNull' => null,
            'nullableSpecificArrayWithArray' => $testArray,
        ];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertNull($target->nullableSpecificArrayWithNull);
        $this->assertSame($testArray, $target->nullableSpecificArrayWithArray);
    }

    public function testMapAcceptsNullableSpecifiedArrayDefaultValues(): void
    {
        $target = new class () {
            /**
             * @var array<int, string>
             */
            public ?array $nullableSpecificArrayWithNull = null;

            /**
             * @var array<int, string>
             */
            public ?array $nullableSpecificArrayWithArray = ['test1', 'test2'];
        };

        $testArray = ['test1', 'test2'];
        $source = [];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertNull($target->nullableSpecificArrayWithNull);
        $this->assertSame($testArray, $target->nullableSpecificArrayWithArray);
    }

    /**
     * @noinspection PhpUnused The test does not access the anonymous class property
     */
    public function testMapDoesNotAcceptUnspecifiedArray(): void
    {
        $target = new class () {
            public array $unspecifiedArray;
        };

        $source = ['unspecifiedArray' => ['test1', 'test2']];

        $this->expectException(TargetPropertyInspectionException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    /**
     * @noinspection PhpUnused The test does not access the anonymous class property
     */
    public function testMapDoesNotAcceptUnexpectedMixedArrayValueTypes(): void
    {
        $target = new class () {
            /**
             * @var string[]
             */
            public array $specifiedArray;
        };

        $source = ['specifiedArray' => ['test1', new stdClass()]];

        $this->expectException(TargetPropertyInspectionException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    /**
     * @noinspection PhpUnused The test does not access the anonymous class property
     */
    public function testMapDoesNotAcceptUnexpectedMixedObjectArrayValueTypes(): void
    {
        $target = new class () {
            /**
             * @var array<int, stdClass>
             */
            public array $specifiedArray;
        };

        $source = ['specifiedArray' => [new stdClass(), 'test1']];

        $this->expectException(SourcePropertyValidationException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    /**
     * @noinspection PhpUnused The test does not access the anonymous class property
     */
    public function testMapHandlesSpecificObjectArrayTypes(): void
    {
        $target = new class () {
            /**
             * @var array<int, stdClass>
             */
            public array $specifiedArray;
        };

        $result = [new stdClass(), new stdClass()];
        $source = ['specifiedArray' => $result];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertSame($result, $target->specifiedArray);
    }

    /**
     * @noinspection PhpUnused The test does not access the anonymous class property
     */
    public function testValidationErrorDoesNotHaveSideEffectsOnOriginalTargetState(): void
    {
        $target = new class () {
            // This is inspected first and valid.
            public int $a = 1;

            // This is inspected second and invalid.
            public int $z = 0;
        };

        $source = ['a' => 2, 'z' => 'invalid'];

        try {
            $this->dataMappingService->mapArrayOntoObject($source, $target);
        } catch (SourcePropertyValidationException $e) {
            $this->assertSame(1, $target->a);
        } catch (TargetPropertyInspectionException $e) {
            // This won't be thrown
        }
    }

    public function testMapRawJson(): void
    {
        $rawJson = '{"someInt":1}';

        $target = new class () {
            public int $someInt;
        };

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapRawJsonOntoObject($rawJson, $target);

        $this->assertSame(1, $target->someInt);
    }

    public function testMapRawJsonDoesNotAcceptInvalidJson(): void
    {
        $rawJson = '{"someKey"="SomeValue"}';

        $this->expectException(SourcePropertyValidationException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapRawJsonOntoObject($rawJson, new stdClass());
    }

    public function testMapSimpleXmlElement(): void
    {
        $result = 'hello';
        $simpleXMLElement = new SimpleXMLElement(
            "<someClass><someString>{$result}</someString></someClass>"
        );

        $target = new class () {
            public string $someString;
        };

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapSimpleXmlElementOntoObject(
            $simpleXMLElement,
            $target
        );

        $this->assertSame($result, $target->someString);
    }

    public function testMapRawXml(): void
    {
        $result = 'hello';
        $rawXml = "<someClass><someString>{$result}</someString></someClass>";

        $target = new class () {
            public string $someString;
        };

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapRawXmlOntoObject($rawXml, $target);

        $this->assertSame($result, $target->someString);
    }

    /**
     * @noinspection PhpUnused The test does not access the anonymous class property
     */
    public function testMapRawXmlDoesNotAcceptInvalidXml(): void
    {
        $result = 'hello';
        $rawXml = "<someClass><someString>{$result}<//someString></someClass>";

        $target = new class () {
            public string $someString;
        };

        $this->expectException(SourcePropertyValidationException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapRawXmlOntoObject($rawXml, $target);
    }

    public function testMapStdClassObject(): void
    {
        $target = new class () {
            public int $someInt;
        };

        $sourceObject = new stdClass();
        $sourceObject->someInt = 1;

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapStdClassOntoObject($sourceObject, $target);

        $this->assertSame(1, $target->someInt);
    }

    /** @noinspection PhpUnused Target property access is not in focus of this test */
    public function testSecondMappingUsesCacheAndThereforeIsFaster(): void
    {
        /*
         * Simply initializing the variables,
         * so they won't manipulate the time measurement
         */
        /** @noinspection PhpUnusedLocalVariableInspection */
        $startTime = 0.0;
        /** @noinspection PhpUnusedLocalVariableInspection */
        $endTime = 0.0;

        $target = new class () {
            public string $someString;
        };

        $source = [
            'someString' => 'someValue'
        ];

        $startTime = microtime(true);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
        $endTime = microtime(true);

        $timeWithoutCache = $endTime - $startTime;

        $startTime = microtime(true);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
        $endTime = microtime(true);

        $timeWithCache = $endTime - $startTime;

        $this->assertLessThanOrEqual($timeWithoutCache * 20, $timeWithCache);
    }

    public function testChildClassVariablesOverwriteParentClassPropertiesInCache(): void
    {
        $targetChildClass = new class () extends AbstractClassImplementation {
            public string $someString = 'child';
        };

        $source = [];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $targetChildClass);

        $this->assertSame('child', $targetChildClass->someString);
    }

    public function testAbstractClassDependencyInArrayRepresentationIsMapped(): void
    {
        $target = new class {
            public AbstractClass $abstract;
        };

        $source = ['abstract' => ['someString' => 'yeah!']];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        $this->assertInstanceOf(AbstractClass::class, $target->abstract);
        $this->assertSame('yeah!', $target->abstract->someString);
    }

    public function testAbstractClassArrayDependencyInArrayRepresentationIsMapped(): void
    {
        $target = new class {
            /**
             * @var array<int, AbstractClass>
             */
            public array $abstract;
        };

        $source = ['abstract' => [
                ['someString' => 'yeah!'],
                new class () extends AbstractClass {
                }
            ]
        ];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);

        foreach ($target->abstract as $abstractInstance) {
            $this->assertInstanceOf(AbstractClass::class, $abstractInstance);
        }

        $this->assertSame('yeah!', $target->abstract[0]->someString);
    }

    public function testDoesntAcceptInvalidSourceNamingConvention(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new DataMappingService(-27345);
    }

    /** @noinspection PhpUnusedPrivateFieldInspection */
    public function testServiceDoesntAcceptUnallowedNamingConventions(): void
    {
        $this->dataMappingService = new DataMappingService(
            SourceNamingConventions::LOWER_SNAKE_CASE
        );

        $target = new class () {
            private string $bla;
        };

        $source = ['Bla' => 'someValue'];

        $this->expectException(SourcePropertyValidationException::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
    }

    public function testServiceTagesDefaultValueWhenIfNoNamingConventionMatches(): void
    {
        $this->dataMappingService = new DataMappingService(
            SourceNamingConventions::LOWER_SNAKE_CASE
        );

        $target = new class () {
            public string $bla = 'initialValue';
        };

        $source = ['Bla' => 'someValue'];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
        $this->assertSame('initialValue', $target->bla);
    }

    public function testSetIsUsingTargetObjectSetters(): void
    {
        $targetObject = new class () {
            private string $string;

            public int $int;

            /** @noinspection PhpUnused Used by data mapping service in this test */
            public function setString(string $string): void
            {
                $this->string = $string . 'Peter';
            }

            public function getString(): string
            {
                return $this->string;
            }
        };

        $source = ['string' => 'someValue', 'int' => 5];

        $this->dataMappingService->setIsUsingTargetObjectSetters(
            true
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $targetObject);

        $this->assertSame('someValuePeter', $targetObject->getString());
        $this->assertSame(5, $targetObject->int);

        $this->dataMappingService->setIsUsingTargetObjectSetters(false);

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $targetObject);

        $this->assertSame('someValue', $targetObject->getString());
    }

    /** @noinspection PhpUnusedPrivateFieldInspection */
    public function testWithAllNamingConventions(): void
    {
        $this->dataMappingService = new DataMappingService(
            SourceNamingConventions::LOWER_SNAKE_CASE
            | SourceNamingConventions::CAMEL_CASE
            | SourceNamingConventions::KEBAP_CASE
            | SourceNamingConventions::PASCAL_CASE
            | SourceNamingConventions::UPPER_SNAKE_CASE
        );

        $target = new class () {
            private string $lowerSnakeCase;

            private string $camel_case;

            private string $kebapCase;

            private string $pascalCase;

            private string $upper_snake_case;
        };

        $source = [
            'lower_snake_case' => 'sourceValue',
            'camelCase' => 'sourceValue',
            'kebap-case' => 'sourceValue',
            'PascalCase' => 'sourceValue',
            'UPPER_SNAKE_CASE' => 'sourceValue'
        ];

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->dataMappingService->mapArrayOntoObject($source, $target);
        $reflectionOfTarget = new ReflectionObject($target);

        foreach ($reflectionOfTarget->getProperties() as $property) {
            $property->setAccessible(true);
            $this->assertSame('sourceValue', $property->getValue($target));
        }
    }
}
