<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Cache;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\Collection\CachedProperty;
use Oktavlachs\DataMappingService\Collection\SourceNamingConventions;
use PHPUnit\Framework\TestCase;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Cache\TargetObjectPropertyInformation;

/**
 * Class TargetObjectPropertyInformationTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Cache
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class TargetObjectPropertyInformationTest extends TestCase
{
    private TargetObjectPropertyInformation $targetPropertyInformationCache;

    /**
     * @var DataMappingService
     */
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
        $this->targetPropertyInformationCache =
            TargetObjectPropertyInformation::getInstance();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            TargetObjectPropertyInformation::class,
            $this->targetPropertyInformationCache
        );
    }

    public function testGet(): void
    {
        // TODO: Move tests from the data mapping service into this class.
        $this->assertTrue(true);
    }
}
