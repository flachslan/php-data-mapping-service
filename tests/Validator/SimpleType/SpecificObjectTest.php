<?php

/** @copyright 2020 Lukas Stermann */

declare(strict_types=1);

namespace Oktavlachs\DataMappingService\Tests\Validator\SimpleType;

use InvalidArgumentException;
use Oktavlachs\DataMappingService\DataMappingService;
use Oktavlachs\DataMappingService\Tests\Dummy\{Class1,
    DummyAbstractClass,
    UseStatements\ClassWithSimpleUseStatement};
use Oktavlachs\DataMappingService\Validator\SimpleType\SpecificObject;
use PHPUnit\Framework\TestCase;

/**
 * Class SpecificObjectTest
 *
 * @package Oktavlachs\DataMappingService\Tests\Validator\SimpleType
 *
 * @author Lukas Stermann <lukas.stermann@googlemail.com>
 */
class SpecificObjectTest extends TestCase
{
    private DataMappingService $dataMappingService;

    public function setUp(): void
    {
        $this->dataMappingService = new DataMappingService();
    }

    public function testConstruct(): void
    {
        $this->assertInstanceOf(
            SpecificObject::class,
            new SpecificObject(Class1::class)
        );
    }

    public function testConstructDoesntAllowNonSpecificObjectTypes(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new SpecificObject('object');
    }

    public function testConstructDoesntAllowAbstractClasses(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new SpecificObject(DummyAbstractClass::class);
    }

    public function testValidate(): void
    {
        $validator = new SpecificObject(ClassWithSimpleUseStatement::class);
        $this->assertInstanceOf(
            ClassWithSimpleUseStatement::class,
            $validator->validate(
                $this->dataMappingService,
                new ClassWithSimpleUseStatement()
            )
        );

        $this->assertInstanceOf(
            ClassWithSimpleUseStatement::class,
            $validator->validate(
                $this->dataMappingService,
                [
                    'someArray' => []
                ]
            )
        );
    }

    public function testValidateWithWrongValue(): void
    {
        $validator = new SpecificObject(ClassWithSimpleUseStatement::class);

        $this->expectException(InvalidArgumentException::class);
        $validator->validate($this->dataMappingService, new Class1());
    }
}
